package com.telerik.ridepal.controllers;

import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.helpers.TimeCalculator;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.services.contracts.DatabaseUpdateService;
import com.telerik.ridepal.services.contracts.PlaylistService;
import com.telerik.ridepal.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final String USER_SUCCESSFULLY_DELETED_MESSAGE = "User with id %d was successfully deleted";
    private static final String PLAYLIST_SUCCESSFULLY_DELETED_MESSAGE = "Playlist with id %d was successfully deleted";
    private static final String PROFILE_PICTURE_SUCCESSFULLY_CHANGED_MESSAGE = "Profile picture was successfully changed";
    private static final String COVER_SUCCESSFULLY_CHANGED_MESSAGE = "Cover was successfully changed";
    public static final String USER_SUCCESSFULLY_UPDATED_MESSAGE = "User with id %d was successfully updated";
    public static final String PLAYLIST_SUCCESSFULLY_UPDATED_MESSAGE = "Playlist with id %d was successfully updated";
    public static final String GENRES_SUCCESSFULLY_UPDATED_MESSAGE = "Genres are successfully updated";
    public static final String ARTISTS_SUCCESSFULLY_UPDATED_MESSAGE = "Artists are successfully updated";
    public static final String ALBUMS_SUCCESSFULLY_UPDATED_MESSAGE = "Albums are successfully updated";
    public static final String TRACKS_SUCCESSFULLY_UPDATED_MESSAGE = "Tracks are successfully updated";
    public static final String DB_SUCCESSFULLY_UPDATED_MESSAGE = "Database successfully updated";

    private UserService userService;
    private PlaylistService playlistService;
    private DatabaseUpdateService databaseUpdateService;

    @Autowired
    public AdminController(UserService userService,
                           PlaylistService playlistService,
                           DatabaseUpdateService databaseUpdateService) {
        this.userService = userService;
        this.playlistService = playlistService;
        this.databaseUpdateService = databaseUpdateService;
    }

    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    public String showAccessDenied() {
        return "admin/access-denied";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showAdminPanel(Model model) {

        List<User> users = new ArrayList<>();
        List<Playlist> playlists = new ArrayList<>();
        TimeCalculator timeCalculator = new TimeCalculator();

        try {
            users = userService.getAllUsers();
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
        }
        model.addAttribute("users", users);

        try {
            playlists = playlistService.getPlaylists();
        } catch (EntityNotFoundException e) {
            model.addAttribute("PlaylistError", e.getMessage());
        }
        model.addAttribute("playlists", playlists);
        model.addAttribute("timeCalculator", timeCalculator);

        return "admin/admin-portal";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String showEditUser(@PathVariable int id,
                               Model model) {
        try {
            User user = userService.getUser(id);
            model.addAttribute("user", user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
            return showAdminPanel(model);
        }
        return "admin/edit-user";
    }

    @RequestMapping(value = "/users/confirm-delete/{id}", method = RequestMethod.GET)
    public String showDeleteUserConfirmation(@PathVariable int id,
                                             Model model) {
        try {
            User user = userService.getUser(id);
            model.addAttribute("user", user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
            return showAdminPanel(model);
        }

        return "admin/delete-user";
    }

    @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable int id,
                             Model model) {

        try {
            userService.deleteUser(id);
            model.addAttribute("userMessage",
                    String.format(USER_SUCCESSFULLY_DELETED_MESSAGE, id));
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
        }

        try {
            model.addAttribute("users", userService.getAllUsers());
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
        }

        return showAdminPanel(model);
    }

    @RequestMapping(value = "/users/change-profile-picture/{id}", method = RequestMethod.POST)
    public String changeProfilePicture(@PathVariable int id,
                                       @RequestParam("profilePicture") MultipartFile profilePicture,
                                       Model model) {
        try {
            User user = userService.getUser(id);
            userService.addProfilePicture(user.getUsername(), profilePicture);
            userService.updateUser(user);
            model.addAttribute("userMessage", PROFILE_PICTURE_SUCCESSFULLY_CHANGED_MESSAGE);
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
        }
        return showAdminPanel(model);
    }

    @RequestMapping(value = "/users/edit/{id}", method = RequestMethod.POST)
    public String changePersonalInformation(@PathVariable int id,
                                            @RequestParam("firstName") String firstName,
                                            @RequestParam("lastName") String lastName,
                                            @RequestParam("email") String email,
                                            Model model) {
        try {
            User user = userService.getUser(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            userService.updateUser(user);
            model.addAttribute("userMessage",
                    String.format(USER_SUCCESSFULLY_UPDATED_MESSAGE, id));
        } catch (EntityNotFoundException e) {
            model.addAttribute("userError", e.getMessage());
        }
        return showAdminPanel(model);
    }


    @RequestMapping(value = "/playlists/{id}", method = RequestMethod.GET)
    public String showEditPlaylist(@PathVariable int id,
                                   Model model) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            model.addAttribute("playlist", playlist);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
            return showAdminPanel(model);
        }

        return "admin/edit-playlist";
    }

    @RequestMapping(value = "/playlists/edit/{id}", method = RequestMethod.POST)
    public String changePlaylistInformation(@PathVariable int id,
                                            @RequestParam("title") String title,
                                            @RequestParam("description") String description,
                                            Model model) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            playlist.setTitle(title);
            playlist.setDescription(description);
            playlistService.updatePlaylist(playlist);
            model.addAttribute("playlistMessage",
                    String.format(PLAYLIST_SUCCESSFULLY_UPDATED_MESSAGE, id));
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }
        return showAdminPanel(model);
    }

    @RequestMapping(value = "/playlists/change-cover/{id}", method = RequestMethod.POST)
    public String changeCoverPicture(@PathVariable int id,
                                     Model model) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            playlist.setCover(playlistService.GenerateCoverForPlaylist());
            playlistService.updatePlaylist(playlist);
            model.addAttribute("playlistMessage", COVER_SUCCESSFULLY_CHANGED_MESSAGE);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }
        return showAdminPanel(model);
    }

    @RequestMapping(value = "/playlists/confirm-delete/{id}", method = RequestMethod.GET)
    public String showDeletePlaylistConfirmation(@PathVariable int id,
                                                 Model model) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            model.addAttribute("playlist", playlist);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
            return showAdminPanel(model);
        }

        return "admin/delete-playlist";
    }

    @RequestMapping(value = "/playlists/delete/{id}", method = RequestMethod.GET)
    public String deletePlaylist(@PathVariable int id,
                                 Model model) {

        try {
            playlistService.deletePlaylist(id);
            model.addAttribute("playlistMessage",
                    String.format(PLAYLIST_SUCCESSFULLY_DELETED_MESSAGE, id));
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }

        try {
            model.addAttribute("playlists", playlistService.getPlaylists());
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());

        }
        return showAdminPanel(model);
    }

    @RequestMapping(value = "/syncGenres", method = RequestMethod.GET)
    public String syncGenres(Model model) {

        databaseUpdateService.updateGenres();
        model.addAttribute("dbMessage", GENRES_SUCCESSFULLY_UPDATED_MESSAGE);

        return showAdminPanel(model);
    }

    @RequestMapping(value = "/syncArtists", method = RequestMethod.GET)
    public String syncArtists(Model model) {

        databaseUpdateService.updateArtists();
        model.addAttribute("dbMessage", ARTISTS_SUCCESSFULLY_UPDATED_MESSAGE);

        return showAdminPanel(model);
    }

    @RequestMapping(value = "/syncAlbums", method = RequestMethod.GET)
    public String syncAlbums(Model model) {

        databaseUpdateService.updateAlbums();
        model.addAttribute("dbMessage", ALBUMS_SUCCESSFULLY_UPDATED_MESSAGE);

        return showAdminPanel(model);
    }

    @RequestMapping(value = "/syncTracks", method = RequestMethod.GET)
    public String syncTracks(Model model) {

        databaseUpdateService.updateTracks();
        model.addAttribute("dbMessage", TRACKS_SUCCESSFULLY_UPDATED_MESSAGE);

        return showAdminPanel(model);
    }

    @RequestMapping(value = "/updateDB", method = RequestMethod.GET)
    public String syncDB(Model model) {

        databaseUpdateService.updateDatabase();
        model.addAttribute("dbMessage", DB_SUCCESSFULLY_UPDATED_MESSAGE);

        return showAdminPanel(model);
    }
}
