package com.telerik.ridepal.controllers;

import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.helpers.TimeCalculator;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.repositories.contracts.PlaylistRepository;
import com.telerik.ridepal.services.contracts.PlaylistService;
import com.telerik.ridepal.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
public class HomeController {
    private static final String NO_PLAYLISTS_ERROR_MESSAGE = "You haven't created any playlists yet";

    private UserService userService;
    private PlaylistRepository playlistRepository;
    private PlaylistService playlistService;

    @Autowired
    public HomeController(UserService userService, PlaylistRepository playlistRepository, PlaylistService playlistService) {
        this.userService = userService;
        this.playlistRepository = playlistRepository;
        this.playlistService = playlistService;
    }

    @GetMapping("/")
    public String welcomePage() {
        return "index";
    }

    @GetMapping("/home")
    public String homePage(Model model, Principal principal) {
        List<Playlist> top3Playlists = playlistRepository.getPTop3Playlists();
        model.addAttribute("playlists", top3Playlists);
        TimeCalculator timeCalculator = new TimeCalculator();
        int totalPlayTime = 0;

        try {
            totalPlayTime = calculateTotalPlayTime(top3Playlists);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", NO_PLAYLISTS_ERROR_MESSAGE);
        }

        if (principal != null) {
            User user = userService.getUser(principal.getName());
            model.addAttribute("user", user);
        }


        model.addAttribute("timeCalculator", timeCalculator);
        model.addAttribute("timeCalculator", timeCalculator);

        return "home";
    }

    @GetMapping("/welcome")
    public String welcome(Principal principal, Model model) {
        User user = userService.getUser(principal.getName());
        model.addAttribute("user", user);
        return "welcome-user";
    }

    private static int calculateTotalPlayTime(List<Playlist> list) {
        Iterator<Playlist> it = list.iterator();
        int res = 0;
        while (it.hasNext()) {
            int num = it.next().getDuration();
            res += num;
        }
        return res;
    }
}
