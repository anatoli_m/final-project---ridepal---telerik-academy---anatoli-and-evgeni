package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Genre;
import com.telerik.ridepal.repositories.contracts.GenreRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.ridepal.Factory.createGenre;
import static com.telerik.ridepal.Factory.genresResponse;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class GenreServiceImplTests {

    @InjectMocks
    GenreServiceImpl genreService;

    @Mock
    GenreRepository genreRepository;

    @Mock
    RestTemplate restTemplate;

    @Test
    public void uploadGenresShould_CallRepository() {
        //Arrange
        String url = "some url";
        Mockito.when(restTemplate.getForObject(url, String.class))
                .thenReturn(genresResponse());

        //Act
        genreService.uploadGenres(url);

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1))
                .saveAll(any(List.class));
    }

    @Test
    public void createGenreShould_CallRepository() {
        //Arrange
        Genre genre = createGenre();

        //Act
        genreService.createGenre(genre);

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1)).save(genre);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createGenreShould_Throw_WhenGenreIdAlreadyExists() {
        //Arrange
        Genre genre = createGenre();
        Mockito.when(genreRepository.existsById(genre.getId())).thenReturn(true);

        //Act
        genreService.createGenre(genre);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createGenreShould_Throw_WhenGenreNameAlreadyExists() {
        //Arrange
        Genre genre = createGenre();
        Mockito.when(genreRepository.existsById(genre.getId())).thenReturn(false);
        Mockito.when(genreRepository.getGenreByName(createGenre().getName())).thenReturn(genre);

        //Act
        genreService.createGenre(genre);
    }

    @Test
    public void createGenreShould_CreateGenre() {
        //Arrange
        Genre expectedGenre = createGenre();
        Mockito.when(genreRepository.save(expectedGenre)).thenReturn(expectedGenre);

        //Act
        Genre actualGenre = genreService.createGenre(expectedGenre);

        //Assert
        Assert.assertSame(actualGenre, expectedGenre);
    }

    @Test
    public void updateGenreShould_CallRepository() {
        //Arrange
        Genre genre = createGenre();
        Mockito.when(genreRepository.getGenreByName(genre.getName())).thenReturn(genre);

        //Act
        genreService.updateGenre(genre);

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1)).save(genre);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateGenreShould_Throw_WhenGenreDoesNotExists() {
        //Act
        genreService.updateGenre(createGenre());
    }

    @Test
    public void updateGenreShould_UpdateGenre() {
        //Arrange
        Genre expectedGenre = createGenre();
        Mockito.when(genreRepository.getGenreByName(expectedGenre.getName())).thenReturn(expectedGenre);

        //Act
        Genre actualGenre = genreService.updateGenre(expectedGenre);

        //Assert
        Assert.assertSame(expectedGenre, actualGenre);
    }

    @Test
    public void deleteGenreShould_CallRepository() {
        //Arrange
        Genre genre = createGenre();
        Mockito.when(genreRepository.existsById(genre.getId())).thenReturn(true);

        //Act
        genreService.deleteGenre(genre.getId());

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1))
                .deleteById(genre.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteGenreShould_Throw_WhenGenreDoesNotExists() {
        //Act
        genreService.deleteGenre(-1);
    }

    @Test
    public void getGenresShould_CallRepository() {
        //Arrange
        List<Genre> genres = new ArrayList<>();
        genres.add(createGenre());
        Mockito.when(genreRepository.findAll()).thenReturn(genres);

        //Act
        genreService.getGenres();

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1)).findAll();
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenresShould_Throw_WhenNoGenres() {
        //Arrange
        List<Genre> genres = new ArrayList<>();
        Mockito.when(genreRepository.findAll()).thenReturn(genres);

        //Act
        genreService.getGenres();
    }

    @Test
    public void getGenresShould_ReturnGenres() {
        //Arrange
        List<Genre> expectedGenres = new ArrayList<>();
        expectedGenres.add(createGenre());
        Mockito.when(genreRepository.findAll()).thenReturn(expectedGenres);

        //Act
        List<Genre> actualGenres = genreService.getGenres();

        //Assert
        Assert.assertSame(actualGenres.size(), expectedGenres.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenreByIdShould_Throw_WhenGenreDoesNotExists() {
        //Act
        genreService.getGenreById(-1);
    }

    @Test
    public void getGenreByNameShould_CallRepository() {
        //Arrange
        Genre genre = createGenre();
        Mockito.when(genreRepository.getGenreByName(genre.getName())).thenReturn(genre);

        //Act
        genreService.getGenreByName(genre.getName());

        //Assert
        Mockito.verify(genreRepository, Mockito.times(2))
                .getGenreByName(genre.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenreByNameShould_Throw_WhenGenreDoesNotExists() {
        //Act
        genreService.getGenreByName(anyString());
    }

    @Test
    public void getGenreByNameShould_ReturnGenre() {
        //Arrange
        Genre expectedGenre = createGenre();
        Mockito.when(genreRepository.getGenreByName(expectedGenre.getName())).thenReturn(expectedGenre);

        //Act
        Genre actualGenre = genreService.getGenreByName(expectedGenre.getName());

        //Assert
        Assert.assertSame(actualGenre, expectedGenre);
    }

    @Test
    public void checkIfGenreExistsIDShould_CallRepository() {

        //Act
        genreService.checkIfGenreExists(anyInt());

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1))
                .existsById(anyInt());
    }

    @Test
    public void checkIfGenreExistsNameShould_CallRepository() {

        //Act
        genreService.checkIfGenreExists(anyString());

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1))
                .getGenreByName(anyString());
    }
}
