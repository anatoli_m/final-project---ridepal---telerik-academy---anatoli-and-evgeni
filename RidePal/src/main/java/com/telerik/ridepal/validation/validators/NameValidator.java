package com.telerik.ridepal.validation.validators;

import com.telerik.ridepal.validation.ValidName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class NameValidator
        implements ConstraintValidator<ValidName, String> {

    private Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{2,20}$");

    @Override
    public boolean isValid(String name, ConstraintValidatorContext context) {
        return pattern.matcher(name).matches();
    }
}