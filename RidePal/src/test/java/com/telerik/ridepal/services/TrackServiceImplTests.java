package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Track;
import com.telerik.ridepal.repositories.contracts.TrackRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.ridepal.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class TrackServiceImplTests {

    @InjectMocks
    TrackServiceImpl trackService;

    @Mock
    TrackRepository trackRepository;

    @Mock
    RestTemplate restTemplate;

    @Test
    public void createTracksShould_CreateTrack() {
        //Arrange
        Track expectedTrack = createTrack();
        Mockito.when(trackRepository.existsById(expectedTrack.getId())).thenReturn(false);

        //Act
        Track actualTrack = trackService.createTrack(expectedTrack);

        //Assert
        Assert.assertSame(expectedTrack, actualTrack);
    }

    @Test
    public void createTracksShould_CallRepository() {
        //Arrange
        Track track = createTrack();
        Mockito.when(trackRepository.existsById(track.getId())).thenReturn(false);

        //Act
        trackService.createTrack(track);

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1)).save(track);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createTrackShould_Throw_WhenTrackAlreadyExists() {
        //Arrange
        Track track = createTrack();
        Mockito.when(trackRepository.existsById(track.getId())).thenReturn(true);

        //Act
        trackService.createTrack(track);
    }

    @Test
    public void uploadTracksShould_CallRepository() {
        //Arrange
        List<Track> tracks = new ArrayList<>();

        //Act
        trackService.uploadTracks(tracks);

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1)).saveAll(tracks);
    }

    @Test
    public void uploadTracksFromAlbumShould_CallRepository() {
        //Arrange
        String url = "some url";
        Mockito.when(restTemplate.getForObject(url, String.class)).thenReturn(tracksResponse());

        //Act
        trackService.uploadTracksFromAlbum(url, createAlbum());

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1)).saveAll(any(List.class));
    }

    @Test
    public void updateTrackShould_UpdateTrack() {
        //Arrange
        Track expectedTrack = createTrack();
        Mockito.when(trackRepository.existsById(expectedTrack.getId())).thenReturn(true);

        //Act
        Track actualTrack = trackService.updateTrack(expectedTrack);

        //Assert
        Assert.assertSame(expectedTrack, actualTrack);
    }

    @Test
    public void updateTrackShould_CallRepository() {
        //Arrange
        Track track = createTrack();
        Mockito.when(trackRepository.existsById(track.getId())).thenReturn(true);

        //Act
        trackService.updateTrack(track);

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1)).save(track);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateTrackShould_Throw_WhenTrackDoesNotExist() {
        //Arrange
        Track track = createTrack();
        Mockito.when(trackRepository.existsById(track.getId())).thenReturn(false);

        //Act
        trackService.updateTrack(track);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteTrackShouldThrow_When_TrackDoesNotExist() {
        //Arrange
        Track track = createTrack();
        Mockito.when(trackRepository.existsById(track.getId())).thenReturn(false);

        //Act
        trackService.deleteTrack(track.getId());
    }

    @Test
    public void deleteTrackShould_CallRepository() {
        //Arrange
        Track track = createTrack();
        Mockito.when(trackRepository.existsById(track.getId())).thenReturn(true);

        //Act
        trackService.deleteTrack(track.getId());

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1))
                .deleteById(track.getId());
    }

    @Test
    public void getTracksShould_ReturnAllTracks() {
        //Arrange
        List<Track> expectedTracks = new ArrayList<>();
        expectedTracks.add(createTrack());
        Mockito.when(trackRepository.findAll()).thenReturn(expectedTracks);

        //Act
        List<Track> actualTracks = trackService.getTracks();

        //Assert
        Assert.assertSame(expectedTracks.size(), actualTracks.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTracksShould_Throw_WhenNoTracks() {
        //Arrange
        List<Track> tracks = new ArrayList<>();
        Mockito.when(trackRepository.findAll()).thenReturn(tracks);

        //Act
        trackService.getTracks();
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByIdShould_Throw_WhenTrackDoesNotExist() {
        //Arrange
        Mockito.when(trackRepository.existsById(anyInt())).thenReturn(false);

        //Act
        trackService.getTrackById(anyInt());
    }

    @Test
    public void getTrackByIdTitleShould_CallRepository() {
        //Arrange
        List<Track> tracks = new ArrayList<>();
        tracks.add(createTrack());
        Mockito.when(trackRepository.getTracksByTitle(anyString())).thenReturn(tracks);

        //Act
        trackService.getTracksByTitle(anyString());

        //Assert
        Mockito.verify(trackRepository, Mockito.times(2))
                .getTracksByTitle(anyString());
    }

    @Test
    public void getTrackByIdTitleShould_ReturnTrack() {
        //Arrange
        List<Track> expectedTracks = new ArrayList<>();
        expectedTracks.add(createTrack());
        Mockito.when(trackRepository.getTracksByTitle(anyString())).thenReturn(expectedTracks);

        //Act
        List<Track> actualTracks = trackService.getTracksByTitle(anyString());

        //Assert
        Assert.assertSame(expectedTracks.size(), actualTracks.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByIdTitleShould_Throw_WhenTrackDoesNotExist() {
        //Arrange
        Track expectedTrack = createTrack();
        Mockito.when(trackRepository.getTracksByTitle(expectedTrack.getTitle())).thenReturn(null);

        //Act
        trackService.getTracksByTitle(expectedTrack.getTitle());
    }

    @Test
    public void getTrackByIdArtistShould_ReturnTrack() {
        //Arrange
        List<Track> expectedTracks = new ArrayList<>();
        expectedTracks.add(createTrack());
        Mockito.when(trackRepository.getTracksByArtist(anyInt())).thenReturn(expectedTracks);

        //Act
        List<Track> actualTracks = trackService.getTrackByArtist(anyInt());

        //Assert
        Assert.assertSame(expectedTracks.size(), actualTracks.size());
    }

    @Test
    public void getTrackByIdArtistShould_CallRepository() {
        //Arrange
        List<Track> tracks = new ArrayList<>();
        tracks.add(createTrack());
        Mockito.when(trackRepository.getTracksByArtist(anyInt())).thenReturn(tracks);

        //Act
        trackService.getTrackByArtist(anyInt());

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1))
                .getTracksByArtist(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByIdArtistShould_Throw_WhenNoTracks() {
        //Arrange
        List<Track> tracks = new ArrayList<>();
        Mockito.when(trackRepository.getTracksByArtist(anyInt())).thenReturn(tracks);

        //Act
        trackService.getTrackByArtist(anyInt());
    }

    @Test
    public void checkIfTrackExistsIdShould_CallRepository() {
        //Act
        trackService.checkIfTrackExists(anyInt());

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1)).existsById(anyInt());
    }

    @Test
    public void checkIfTrackExistsNameShould_CallRepository() {
        //Act
        trackService.checkIfTrackExists(anyString());

        //Assert
        Mockito.verify(trackRepository, Mockito.times(1))
                .getTracksByTitle(anyString());
    }
}
