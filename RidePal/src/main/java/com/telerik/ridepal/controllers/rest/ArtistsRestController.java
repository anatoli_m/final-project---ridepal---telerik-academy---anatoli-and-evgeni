package com.telerik.ridepal.controllers.rest;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.services.contracts.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/artists")
public class ArtistsRestController {

    private ArtistService artistService;

    @Autowired
    public ArtistsRestController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @GetMapping
    public List<Artist> getAllArtists() {
        List<Artist> artists = new ArrayList<>();
        try {
            artists = artistService.getArtists();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return artists;
    }

    @GetMapping("/{id}")
    public Artist getArtist(@PathVariable int id) {
        Artist artist = new Artist();
        try {
            artist = artistService.getArtist(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return artist;
    }

    @PostMapping
    public Artist createArtist(@RequestBody Artist artist) {
        try {
            artistService.createArtist(artist);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return artist;
    }

    @PutMapping
    public Artist updateArtist(@RequestBody Artist artist) {
        try {
            artistService.updateArtist(artist);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return artist;
    }

    @DeleteMapping("/{id}")
    public void deleteArtist(@PathVariable int id) {
        try {
            artistService.deleteArtist(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
