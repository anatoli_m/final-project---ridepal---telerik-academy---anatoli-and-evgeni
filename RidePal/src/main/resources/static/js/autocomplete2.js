$(document).ready(function () {
    $("#endDestination").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "http://dev.virtualearth.net/REST/v1/Locations",
                dataType: "jsonp",
                data: {
                    key: "An416ebZlZez1tWEx8p8mAza-dNI2S3z3qL7gJrxXDBxqfyeUu0XqMjKF_zj3u31",
                    q: request.term
                },
                jsonp: "jsonp",
                success: function (data) {
                    var result = data.resourceSets[0];
                    if (result) {
                        if (result.estimatedTotal > 0) {
                            response($.map(result.resources, function (item) {
                                return {
                                    data: item,
                                    label: item.name + ' (' + item.address.countryRegion + ')',
                                    value: item.name
                                }
                            }));
                        }
                    }
                }
            });
        },
        minLength: 1,
        change: function (event, ui) {
            if (!ui.item)
                $("#endDestination").val('');
        },
        select: function (event, ui) {
            displaySelectedItem(ui.item.data);
        }
    });
});

function displaySelectedItem(item) {
    $("#endDestination").empty().append(item.name);
}