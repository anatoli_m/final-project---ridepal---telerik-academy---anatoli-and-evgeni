package com.telerik.ridepal.services.contracts;

import com.telerik.ridepal.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

    User createUser(User user);

    User updateUser(User user);

    void deleteUser(int id);

    List<User> getAllUsers();

    User getUser(int id);

    User getUser(String username);

    void updateUserDetails(User user, String firstName, String lastName, String email);

    void addProfilePicture(String username, MultipartFile picture);

    void removeProfilePicture(String username);

    void changeUserPassword(User user, String oldPassword, String newPassword, String passwordConfirm);

    boolean checkIfUserExists(int userId);

    boolean checkIfUserExists(String username);

    boolean checkIfEmailExists(String email);

}
