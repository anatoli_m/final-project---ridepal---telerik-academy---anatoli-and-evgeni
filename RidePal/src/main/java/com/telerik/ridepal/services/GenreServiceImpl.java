package com.telerik.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Genre;
import com.telerik.ridepal.repositories.contracts.GenreRepository;
import com.telerik.ridepal.services.contracts.GenreService;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    public static final String GENRE_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Genre with id %d does not exists.";
    public static final String GENRE_WITH_NAME_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "GENRE with name '%s' does not exists.";
    public static final String GENRE_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Genre with id %d already exists.";
    public static final String GENRE_WITH_NAME_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Genre with name '%s' already exists.";
    public static final String NO_GENRES_EXCEPTION_MESSAGE = "There are no genres";

    private GenreRepository genreRepository;
    private RestTemplate restTemplate;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository,
                            RestTemplate restTemplate) {
        this.genreRepository = genreRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public void uploadGenres(String url) {
        List<Genre> genres = fetchGenresFromUrl(url);
        genreRepository.saveAll(genres);
    }

    @Override
    public Genre createGenre(Genre genre) {
        throwIfGenreExists(genre.getId());
        throwIfGenreExists(genre.getName());
        genreRepository.save(genre);
        return genre;
    }

    @Override
    public Genre updateGenre(Genre genre) {
        throwIfGenreDoesNotExists(genre.getName());
        genreRepository.save(genre);
        return genre;
    }

    @Override
    public void deleteGenre(int id) {
        throwIfGenreDoesNotExists(id);
        genreRepository.deleteById(id);
    }

    @Override
    public List<Genre> getGenres() {
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        if (genres.isEmpty()) {
            throw new EntityNotFoundException(NO_GENRES_EXCEPTION_MESSAGE);
        }
        return genres;
    }

    @Override
    public Genre getGenreById(int id) {
        throwIfGenreDoesNotExists(id);
        return genreRepository.findById(id).get();
    }

    @Override
    public Genre getGenreByName(String name) {
        throwIfGenreDoesNotExists(name);
        return genreRepository.getGenreByName(name);
    }

    @Override
    public boolean checkIfGenreExists(int id) {
        return genreRepository.existsById(id);
    }

    @Override
    public boolean checkIfGenreExists(String name) {
        Genre genre = genreRepository.getGenreByName(name);
        return genre != null;
    }

    private List<Genre> fetchGenresFromUrl(String url) {
        String response = restTemplate.getForObject(url, String.class);
        JSONObject jsonObject = new JSONObject(response);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Genre> genres = new ArrayList<>();
        try {
            genres = objectMapper
                    .readValue(jsonObject.get("data").toString(), new TypeReference<List<Genre>>() {
                    });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return genres;
    }

    private void throwIfGenreDoesNotExists(int id) {
        if (!checkIfGenreExists(id)) {
            throw new EntityNotFoundException(
                    String.format(GENRE_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfGenreDoesNotExists(String name) {
        if (!checkIfGenreExists(name)) {
            throw new EntityNotFoundException(
                    String.format(GENRE_WITH_NAME_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, name));
        }
    }

    private void throwIfGenreExists(int id) {
        if (checkIfGenreExists(id)) {
            throw new DuplicateEntityException(
                    String.format(GENRE_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfGenreExists(String name) {
        if (checkIfGenreExists(name)) {
            throw new DuplicateEntityException(
                    String.format(GENRE_WITH_NAME_ALREADY_EXISTS_EXCEPTION_MESSAGE, name));
        }
    }
}
