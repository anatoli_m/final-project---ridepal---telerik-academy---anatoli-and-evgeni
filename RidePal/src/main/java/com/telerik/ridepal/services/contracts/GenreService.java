package com.telerik.ridepal.services.contracts;

import com.telerik.ridepal.models.Genre;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.Track;

import java.util.List;

public interface GenreService {

    void uploadGenres(String url);

    Genre createGenre(Genre genre);

    Genre updateGenre(Genre genre);

    void deleteGenre(int id);

    List<Genre> getGenres();

    Genre getGenreById(int id);

    Genre getGenreByName(String name);

    boolean checkIfGenreExists(int id);

    boolean checkIfGenreExists(String name);
}
