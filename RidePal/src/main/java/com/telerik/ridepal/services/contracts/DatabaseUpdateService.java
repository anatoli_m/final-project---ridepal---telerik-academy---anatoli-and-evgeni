package com.telerik.ridepal.services.contracts;

import org.springframework.stereotype.Service;

@Service
public interface DatabaseUpdateService {

    void updateDatabase();

    void updateGenres();

    void updateArtists();

    void updateAlbums();

    void updateTracks();
}
