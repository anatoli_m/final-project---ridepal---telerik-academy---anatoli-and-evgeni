package com.telerik.ridepal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "artists")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Artist {

    @Id
    @Column(name = "artist_id")
    private int id;

    @NotNull
    @Column(name = "artist_name")
    private String name;

    @Column(name = "picture")
    private String picture_big;

    @Column(name = "tracklist")
    private String tracklist;

    @JsonIgnore
    @Column(name = "enabled")
    private boolean enabled = true;

}
