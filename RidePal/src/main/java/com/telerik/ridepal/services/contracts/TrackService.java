package com.telerik.ridepal.services.contracts;

import com.telerik.ridepal.models.Album;
import com.telerik.ridepal.models.Track;

import java.util.List;

public interface TrackService {

    void uploadTracks(List<Track> tracks);

    void uploadTracksFromAlbum(String url, Album album);

    Track createTrack(Track track);

    Track updateTrack(Track track);

    void deleteTrack(int id);

    List<Track> getTracks();

    Track getTrackById(int id);

    List<Track> getTracksByTitle(String title);

    List<Track> getTrackByArtist(int id);

    boolean checkIfTrackExists(int id);

    boolean checkIfTrackExists(String title);
}
