package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.exceptions.InvalidPasswordException;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.repositories.contracts.UserRepository;
import com.telerik.ridepal.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final String USER_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "User with username '%s' already exists. Please use a different username";
    private static final String EMAIL_ALREADY_EXISTS_EXCEPTION_MESSAGE = "This email is already in use. Please log in";
    private static final String CANT_FIND_USER_EXCEPTION_MESSAGE = "Can't find user with id %d";
    private static final String NO_USERS_EXCEPTION_MESSAGE = "There are no registered users.";
    private static final String CANT_FIND_USER_WITH_USERNAME_EXCEPTION_MESSAGE = "Can't find user with username '%s'";
    private static final String WRONG_PASSWORD_EXCEPTION_MESSAGE = "Wrong password";
    private static final String PASSWORDS_DOES_NOT_MATCH_EXCEPTION_MESSAGE = "Passwords does not match";

    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(User user) {
        throwIfUsernameAlreadyExists(user.getUsername());
        throwIfEmailAlreadyExists(user.getEmail());
        userRepository.save(user);
        return user;
    }

    @Override
    public User updateUser(User user) {
        throwIfUserDoesNotExists(user.getUser_id());
        userRepository.save(user);
        return user;
    }

    @Override
    public void deleteUser(int id) {
        throwIfUserDoesNotExists(id);
        userRepository.deleteUser(id);
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        if (users.isEmpty()) {
            throw new EntityNotFoundException(NO_USERS_EXCEPTION_MESSAGE);
        }
        return users;
    }

    @Override
    public User getUser(int id) {
        throwIfUserDoesNotExists(id);
        return userRepository.findById(id).get();
    }

    @Override
    public User getUser(String username) {
        throwIfUserDoesNotExists(username);
        return userRepository.getUserByUsername(username);
    }

    @Override
    public void updateUserDetails(User user, String firstName, String lastName, String email) {
        throwIfUserDoesNotExists(user.getUser_id());

        if (!user.getEmail().equals(email) &&
                userRepository.getUserByEmail(email) != null) {
            throw new DuplicateEntityException(EMAIL_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        userRepository.save(user);
    }

    @Override
    public void addProfilePicture(String username, MultipartFile picture) {
        User user = getUser(username);
        try {
            byte[] pictureBytes = picture.getBytes();
            String encodedPicture = Base64.getEncoder().encodeToString(pictureBytes);
            user.setPicture(encodedPicture);
        } catch (IOException e) {
            e.printStackTrace();
        }
        userRepository.save(user);
    }

    @Override
    public void removeProfilePicture(String username) {
        User user = getUser(username);
        user.setPicture(null);
        userRepository.save(user);
    }

    @Override
    public void changeUserPassword(User user, String oldPassword, String newPassword, String passwordConfirm) {
        if (!newPassword.equals(passwordConfirm)) {
            throw new InvalidPasswordException(PASSWORDS_DOES_NOT_MATCH_EXCEPTION_MESSAGE);
        }

        boolean oldPasswordMatched = passwordEncoder.matches(oldPassword, user.getPassword());
        if (oldPasswordMatched) {
            user.setPassword(passwordEncoder.encode(newPassword));
            userRepository.save(user);
        } else {
            throw new InvalidPasswordException(WRONG_PASSWORD_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public boolean checkIfUserExists(int userId) {
        return userRepository.existsById(userId);
    }

    @Override
    public boolean checkIfUserExists(String username) {
        User user = userRepository.getUserByUsername(username);
        return user != null;
    }

    @Override
    public boolean checkIfEmailExists(String email) {
        User user = userRepository.getUserByEmail(email);
        return user != null;
    }

    private void throwIfUsernameAlreadyExists(String username) {
        if (checkIfUserExists(username)) {
            throw new DuplicateEntityException(String.format(USER_ALREADY_EXISTS_EXCEPTION_MESSAGE, username));
        }
    }

    private void throwIfEmailAlreadyExists(String email) {
        if (checkIfEmailExists(email)) {
            throw new DuplicateEntityException(EMAIL_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }
    }

    private void throwIfUserDoesNotExists(int id) {
        if (!checkIfUserExists(id)) {
            throw new EntityNotFoundException(String.format(CANT_FIND_USER_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfUserDoesNotExists(String username) {
        if (!checkIfUserExists(username)) {
            throw new EntityNotFoundException(String.format(CANT_FIND_USER_WITH_USERNAME_EXCEPTION_MESSAGE, username));
        }
    }
}
