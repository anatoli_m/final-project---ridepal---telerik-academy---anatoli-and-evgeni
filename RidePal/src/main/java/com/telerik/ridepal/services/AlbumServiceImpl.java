package com.telerik.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Album;
import com.telerik.ridepal.repositories.contracts.AlbumRepository;
import com.telerik.ridepal.services.contracts.AlbumService;
import com.telerik.ridepal.services.contracts.ArtistService;
import com.telerik.ridepal.services.contracts.GenreService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlbumServiceImpl implements AlbumService {

    private static final String ALBUM_TITLE_ALREADY_EXISTS_EXCEPTION_MESSAGE = "Album with title '%s' already exists";
    private static final String ALBUM_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE = "Album with id %d already exists";
    private static final String ALBUM_DOES_NOT_EXISTS_EXCEPTION_MESSAGE = "Album with id %d does not exists";
    private static final String ALBUM_WITH_TITLE_DOES_NOT_EXISTS_EXCEPTION_MESSAGE = "Album with title '%s' does not exists";
    private static final String NO_ALBUMS_EXCEPTION_MESSAGE = "There are no albums in database";
    private static final String ARTIST_DOES_NOT_EXISTS_EXCEPTION_MESSAGE = "Can not find artist with id %d";

    private AlbumRepository albumRepository;
    private ArtistService artistService;
    private GenreService genreService;
    private RestTemplate restTemplate;

    @Autowired
    public AlbumServiceImpl(AlbumRepository albumRepository,
                            RestTemplate restTemplate,
                            GenreService genreService,
                            ArtistService artistService) {
        this.albumRepository = albumRepository;
        this.restTemplate = restTemplate;
        this.genreService = genreService;
        this.artistService = artistService;
    }

    @Override
    public Album createAlbum(Album album) {
        throwIfAlbumIdAlreadyExists(album.getId());
        throwIfAlbumTitleAlreadyExists(album.getTitle());
        albumRepository.save(album);
        return album;
    }

    @Override
    public Album updateAlbum(Album album) {
        throwIfAlbumIdDoesNotExists(album.getId());
        albumRepository.save(album);
        return album;
    }

    @Override
    public void deleteAlbum(int id) {
        throwIfAlbumIdDoesNotExists(id);
        albumRepository.delete(albumRepository.findById(id).get());
    }

    @Override
    public List<Album> getAlbums() {
        List<Album> albums = new ArrayList<>();
        albumRepository.findAll().forEach(albums::add);
        if (albums.isEmpty()) {
            throw new EntityNotFoundException(NO_ALBUMS_EXCEPTION_MESSAGE);
        }
        return albums;
    }

    @Override
    public Album getAlbum(int id) {
        throwIfAlbumIdDoesNotExists(id);
        return albumRepository.findById(id).get();
    }

    @Override
    public Album getAlbum(String title) {
        throwIfAlbumTitleDoesNotExists(title);
        return albumRepository.getAlbumByTitle(title);
    }

    @Override
    public List<Album> getAlbumsByArtist(int id) {
        if (!artistService.checkIfArtistExists(id)) {
            throw new EntityNotFoundException(
                    String.format(ARTIST_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, id));
        }
        return albumRepository.getAlbumsByArtist(id);
    }

    @Override
    public void uploadArtistAlbums(String url, int artistId) {
        List<Album> albums = fetchAlbumsFromUrl(url);
        for (Album album : albums) {
            album.setArtist_id(artistId);
            if (!checkIfAlbumExists(album.getTitle())
                    && genreService.checkIfGenreExists(album.getGenre_id())) {
                albumRepository.save(album);
            }
        }
    }

    @Override
    public boolean checkIfAlbumExists(int id) {
        return albumRepository.existsById(id);
    }

    @Override
    public boolean checkIfAlbumExists(String title) {
        Album album = albumRepository.getAlbumByTitle(title);
        return album != null;
    }

    private List<Album> fetchAlbumsFromUrl(String url) {
        String response = restTemplate.getForObject(url, String.class);
        JSONObject jsonObject = new JSONObject(response);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Album> albums = new ArrayList<>();
        try {
            albums = objectMapper
                    .readValue(jsonObject.get("data").toString(), new TypeReference<List<Album>>() {
                    });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return albums;
    }

    private void throwIfAlbumTitleAlreadyExists(String title) {
        if (checkIfAlbumExists(title)) {
            throw new DuplicateEntityException(
                    String.format(ALBUM_TITLE_ALREADY_EXISTS_EXCEPTION_MESSAGE, title));
        }
    }

    private void throwIfAlbumIdAlreadyExists(int id) {
        if (checkIfAlbumExists(id)) {
            throw new DuplicateEntityException(
                    String.format(ALBUM_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfAlbumIdDoesNotExists(int id) {
        if (!checkIfAlbumExists(id)) {
            throw new EntityNotFoundException(
                    String.format(ALBUM_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfAlbumTitleDoesNotExists(String title) {
        if (!checkIfAlbumExists(title)) {
            throw new EntityNotFoundException(
                    String.format(ALBUM_WITH_TITLE_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, title));
        }
    }
}
