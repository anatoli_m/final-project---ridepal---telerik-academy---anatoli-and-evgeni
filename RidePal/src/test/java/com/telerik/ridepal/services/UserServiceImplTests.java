package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.exceptions.InvalidPasswordException;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static com.telerik.ridepal.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    BCryptPasswordEncoder passwordEncoder;

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl mockUserService;

    @Test
    public void getAllUsersShould_CallRepository() {
        //Arrange
        List<User> users = new ArrayList<>();
        users.add(createUser());
        Mockito.when(mockUserRepository.findAll()).thenReturn(users);

        //Act
        mockUserService.getAllUsers();

        //Assert
        Mockito.verify(mockUserRepository,
                times(1)).findAll();
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUserShouldThrow_When_UserNameAlreadyExists() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.getUserByUsername(anyString()))
                .thenReturn(user);

        //Act
        mockUserService.createUser(createUser());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUserShouldThrow_When_UserEmailAlreadyExists() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.getUserByEmail(anyString()))
                .thenReturn(user);

        //Act
        mockUserService.createUser(createUser());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAllUsersShould_Throw_When_NoUsers() {
        //Assert
        Assert.assertTrue(mockUserService.getAllUsers().isEmpty());
    }

    @Test
    public void getAllUsersShould_ReturnAllUsers() {
        //Arrange
        List<User> expectedList = new ArrayList();
        expectedList.add(createUser());
        Mockito.when(mockUserRepository.findAll()).thenReturn(expectedList);

        //Act
        List<User> actualList = mockUserService.getAllUsers();

        //Assert
        Assert.assertEquals(expectedList, actualList);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByIdShouldThrow_WhenUserDoestNotExist() {
        //Act
        mockUserService.getUser(anyInt());
    }

    @Test
    public void getUserByUserNameShould_CallRepository() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.getUserByUsername(user.getUsername()))
                .thenReturn(user);

        //Act
        mockUserService.getUser(user.getUsername());

        //Assert
        Mockito.verify(mockUserRepository,
                times(2)).getUserByUsername(user.getUsername());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByUserNameShouldThrow_WhenUserDoestNotExist() {
        //Act
        mockUserService.getUser(anyInt());
    }

    @Test
    public void getUserByUserNameShould_ReturnUser_WhenUserExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.getUserByUsername(anyString()))
                .thenReturn(expectedUser);

        //Act
        User returnedUser = mockUserService.getUser(anyString());

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test
    public void updateUserShould_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.existsById(anyInt()))
                .thenReturn(true);

        //Act
        mockUserService.updateUser(createUser());

        //Assert
        Mockito.verify(mockUserRepository, times(1))
                .save(any(User.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateUserShouldThrow_WhenUserDoesNotExist() {
        //Act
        mockUserService.updateUser(createUser());
    }

    @Test
    public void updateUserShould_ReturnUpdatedUsername() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.existsById(anyInt()))
                .thenReturn(true);
        Mockito.when(mockUserRepository.save(any(User.class)))
                .thenReturn(expectedUser);

        //Act
        User returnedUser = mockUserService.updateUser(expectedUser);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test
    public void updateUserDetailsShould_CallRepository() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.existsById(anyInt())).thenReturn(true);
        Mockito.when(mockUserRepository.save(user)).thenReturn(user);

        //Act
        mockUserService.updateUser(user);

        //Assert
        Mockito.verify(mockUserRepository, times(1))
                .save(any(User.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateUserDetailsShould_ThrowIfUserDoesNotExist() {
        //Act
        mockUserService.updateUserDetails(createUser(),
                "Name", "Name", "email");
    }

    @Test
    public void updateUserDetailsShould_UpdateUserDetails() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.save(any(User.class))).thenReturn(expectedUser);
        Mockito.when(mockUserRepository.existsById(anyInt())).thenReturn(true);

        //Act
        User actualUser = expectedUser;
        mockUserService.updateUserDetails(actualUser,
                "Maurice", "Laas", "email");

        //Assert
        Assert.assertSame(expectedUser, actualUser);
    }

    @Test
    public void deleteUserShould_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.existsById(anyInt())).thenReturn(true);

        //Act
        mockUserService.deleteUser(anyInt());

        //Assert
        Mockito.verify(mockUserRepository, times(1)).deleteUser(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteUserShouldThrow_When_UserDoesNotExist() {
        //Act
        mockUserService.deleteUser(anyInt());
    }

    @Test
    public void addProfilePictureShould_CallRepository() {
        //Arrange
        User user = createUser();
        MultipartFile picture = new MultipartFile() {
            @Override
            public String getName() {
                return "fileName";
            }

            @Override
            public String getOriginalFilename() {
                return "originalFileName";
            }

            @Override
            public String getContentType() {
                return "png";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "Picture Bytes".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };

        Mockito.when(mockUserRepository.getUserByUsername(user.getUsername())).thenReturn(user);
        Mockito.when(mockUserService.getUser(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.addProfilePicture(user.getUsername(), picture);

        //Assert
        Mockito.verify(mockUserRepository, times(1)).save(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void addProfilePictureShould_Throw_WhenUserDoesNotExist() {
        //Act
        mockUserService.addProfilePicture(createUser().getUsername(),
                any(MultipartFile.class));
    }

    @Test
    public void addProfilePictureShould_AddProfilePicture() {
        //Arrange
        User user = createUser();
        MultipartFile picture = new MultipartFile() {
            @Override
            public String getName() {
                return "fileName";
            }

            @Override
            public String getOriginalFilename() {
                return "originalFileName";
            }

            @Override
            public String getContentType() {
                return "png";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "Picture Bytes".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };
        Mockito.when(mockUserRepository.getUserByUsername(anyString())).thenReturn(user);
        Mockito.when(mockUserService.getUser(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.addProfilePicture(user.getUsername(), picture);

        //Assert
        Assert.assertNotNull(user.getPicture());

    }

    @Test
    public void removeProfilePictureShould_CallRepository() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.getUserByUsername(user.getUsername())).thenReturn(user);
        Mockito.when(mockUserService.getUser(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.removeProfilePicture(user.getUsername());

        //Assert
        Mockito.verify(mockUserRepository, times(1)).save(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeProfilePictureShould_Throw_WhenUserDoesNotExist() {
        //Act
        mockUserService.removeProfilePicture(anyString());
    }

    @Test
    public void removeProfilePictureShould_RemoveProfilePicture() {
        //Arrange
        User user = createUser();
        user.setPicture("picture");
        Mockito.when(mockUserRepository.getUserByUsername(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.removeProfilePicture(user.getUsername());

        //Assert
        Assert.assertNull(user.getPicture());
    }

    @Test
    public void changeUserPasswordShould_CallRepository() {
        //Arrange
        User user = createUser();
        Mockito.when(passwordEncoder.matches("pass", "pass")).thenReturn(true);

        //Act
        mockUserService.changeUserPassword(user, "pass", "pass", "pass");

        //Assert
        Mockito.verify(mockUserRepository, times(1))
                .save(any(User.class));
    }

    @Test(expected = InvalidPasswordException.class)
    public void changeUserPasswordShould_Throw_WhenPasswordConfirmDoesNotMatch() {

        //Act
        mockUserService.changeUserPassword(createUser(), "pass", "pass", "paefefss");
    }

    @Test(expected = InvalidPasswordException.class)
    public void changeUserPasswordShould_Throw_WhenPasswordsDontMatch() {

        //Act
        mockUserService.changeUserPassword(createUser(), "dttrfbg", "pass", "pass");
    }
}
