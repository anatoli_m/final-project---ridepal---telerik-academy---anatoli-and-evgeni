package com.telerik.ridepal.controllers;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.exceptions.InvalidPasswordException;
import com.telerik.ridepal.helpers.TimeCalculator;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.services.contracts.PlaylistService;
import com.telerik.ridepal.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UsersController {

    private static final String PROFILE_PICTURE_SUCCESSFULLY_CHANGED_MESSAGE = "Profile picture was successfully changed";
    private static final String PROFILE_SUCCESSFULLY_UPDATED_MESSAGE = "Profile successfully updated";
    private static final String PASSWORD_SUCCESSFULLY_UPDATED_MESSAGE = "Password successfully updated";
    private static final String NO_PLAYLISTS_MESSAGE = "You haven't created any playlists yet";
    private static final String SAVED_PLAYLISTS_MESSAGE = "You haven't saved any playlists yet";
    public static final String PLAYLIST_SUCCESSFULLY_UPDATED_MESSAGE = "Playlist was successfully updated";


    private UserService userService;
    private PlaylistService playlistService;

    @Autowired
    public UsersController(UserService userService,
                           PlaylistService playlistService) {
        this.userService = userService;
        this.playlistService = playlistService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showUserProfile(Model model,
                                  Principal principal) {

        User user;
        try {
            user = userService.getUser(principal.getName());
        } catch (EntityNotFoundException e) {
            return "redirect:/login?error";
        }

        TimeCalculator timeCalculator = new TimeCalculator();
        List<Playlist> userPlaylists = new ArrayList<>();
        List<Playlist> savedPlaylists = new ArrayList<>();
        int totalPlayTime = 0;

        try {
            userPlaylists = playlistService.getPlaylistByUser(user.getUser_id());
            savedPlaylists = playlistService.getUserSavedPlaylists(user.getUser_id());
            totalPlayTime = calculateTotalPlayTime(userPlaylists);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistsMessage", NO_PLAYLISTS_MESSAGE);
        }

        if (savedPlaylists.size() == 0) {
            model.addAttribute("savedMessage", SAVED_PLAYLISTS_MESSAGE);
        }
        if (userPlaylists.size() == 0) {
            model.addAttribute("playlistsMessage", NO_PLAYLISTS_MESSAGE);
        }

        model.addAttribute("user", user);
        model.addAttribute("playlists", userPlaylists);
        model.addAttribute("savedPlaylists", savedPlaylists);
        model.addAttribute("timeCalculator", timeCalculator);
        model.addAttribute("totalPlayTime", totalPlayTime);

        return "user";
    }

    @RequestMapping(value = "/changePicture", method = RequestMethod.POST)
    public String changeProfilePicture(Principal principal,
                                       Model model,
                                       @RequestParam("profilePicture") MultipartFile profilePicture) {

        try {
            User user = userService.getUser(principal.getName());
            userService.addProfilePicture(user.getUsername(), profilePicture);
            model.addAttribute("profileMessage",
                    PROFILE_PICTURE_SUCCESSFULLY_CHANGED_MESSAGE);
        } catch (EntityNotFoundException e) {
            model.addAttribute("profileError", e.getMessage());
        }

        return "redirect:/user";
    }

    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
    public String updateProfile(Principal principal,
                                Model model,
                                @RequestParam("firstName") String firstName,
                                @RequestParam("lastName") String lastName,
                                @RequestParam("email") String email) {

        try {
            User user = userService.getUser(principal.getName());
            userService.updateUserDetails(user, firstName, lastName, email);
            model.addAttribute("profileMessage", PROFILE_SUCCESSFULLY_UPDATED_MESSAGE);
        } catch (DuplicateEntityException e) {
            model.addAttribute("profileError", e.getMessage());
        }

        return "redirect:/user";
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public String changePassword(Principal principal,
                                 Model model,
                                 @RequestParam("oldPassword") String oldPassword,
                                 @RequestParam("newPassword") String newPassword,
                                 @RequestParam("passwordConfirm") String passwordConfirm) {

        try {
            User user = userService.getUser(principal.getName());
            userService.changeUserPassword(user, oldPassword, newPassword, passwordConfirm);
            model.addAttribute("profileMessage", PASSWORD_SUCCESSFULLY_UPDATED_MESSAGE);
        } catch (InvalidPasswordException e) {
            model.addAttribute("profileError", e.getMessage());
        }

        return "redirect:/user";
    }

    @RequestMapping(value = "/confirm-delete", method = RequestMethod.GET)
    public String showDeleteUserConfirmation(Model model) {

        return "delete-profile-confirmation";
    }

    @RequestMapping(value = "/deleteProfile", method = RequestMethod.GET)
    public String deleteProfile(Principal principal,
                                Model model) {
        try {
            User user = userService.getUser(principal.getName());
            List<Playlist> playlists = playlistService.getPlaylistByUser(user.getUser_id());
            for (Playlist playlist : playlists) {
                playlistService.deletePlaylist(playlist.getPlaylist_id());
            }
            userService.deleteUser(user.getUser_id());
        } catch (EntityNotFoundException e) {
            model.addAttribute("profileError", e.getMessage());
            return showUserProfile(model, principal);
        }

        return "redirect:/login?deleted";
    }

    @RequestMapping(value = "/editPlaylist/{id}", method = RequestMethod.GET)
    public String showEditPlaylist(@PathVariable int id,
                                   Model model,
                                   Principal principal) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            model.addAttribute("playlist", playlist);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
            return showUserProfile(model, principal);
        }

        return "edit-playlist";
    }

    @RequestMapping(value = "/editPlaylist/{id}", method = RequestMethod.POST)
    public String editPlaylist(@PathVariable int id,
                               @RequestParam("title") String title,
                               @RequestParam("description") String description,
                               Model model,
                               Principal principal) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            playlist.setTitle(title);
            playlist.setDescription(description);
            playlistService.updatePlaylist(playlist);
            model.addAttribute("playlistMessage",
                    String.format(PLAYLIST_SUCCESSFULLY_UPDATED_MESSAGE, id));
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }
        return "redirect:/user";
    }

    @RequestMapping(value = "/playlist/changeCover/{id}", method = RequestMethod.GET)
    public String changeCoverPicture(@PathVariable int id,
                                     Model model) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            playlist.setCover(playlistService.GenerateCoverForPlaylist());
            playlistService.updatePlaylist(playlist);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }
        return "redirect:/user";
    }

    @RequestMapping(value = "/playlist/deletePlaylist/{id}", method = RequestMethod.GET)
    public String deletePlaylist(@PathVariable int id,
                                 Model model) {
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            playlistService.deletePlaylist(playlist.getPlaylist_id());
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }
        return "redirect:/user";
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
    public String removePlaylistFromSaved(@PathVariable int id,
                                          Principal principal,
                                          Model model) {
        try {
            User user = userService.getUser(principal.getName());
            user.getSavedPlaylists().removeIf(playlist -> playlist.getPlaylist_id() == id);
            userService.updateUser(user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("playlistError", e.getMessage());
        }
        return "redirect:/user";
    }

    private static int calculateTotalPlayTime(List<Playlist> list) {
        Iterator<Playlist> it = list.iterator();
        int res = 0;
        while (it.hasNext()) {
            int num = it.next().getDuration();
            res += num;
        }
        return res;
    }
}
