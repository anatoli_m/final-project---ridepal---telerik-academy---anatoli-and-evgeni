package com.telerik.ridepal.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class LocationForm {

    private String fromDestination;

    private String toDestination;

    private String title;

    private String description;

    private boolean distinctArtists;

    private boolean includeRemixes;

    private boolean topTracks;

    private boolean includeLivePerformance;

    private boolean makePublic;

    private int hiphop;

    private int pop;

    private int metal;

    private int house;

    private List<Double> centroid;

    private List<Double> coordinates;

    private double duration;
}
