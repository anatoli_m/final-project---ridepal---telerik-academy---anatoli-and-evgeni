package com.telerik.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.repositories.contracts.ArtistRepository;
import com.telerik.ridepal.services.contracts.ArtistService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ArtistServiceImpl implements ArtistService {

    public static final String ARTIST_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Artist with id %d does not exists.";
    public static final String ARTIST_WITH_NAME_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Artist with name '%s' does not exists.";
    public static final String ARTIST_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Artist with id %d already exists.";
    public static final String ARTIST_WITH_NAME_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Artist with name '%s' already exists.";
    public static final String NO_ARTISTS_EXCEPTION_MESSAGE = "There are no artists";

    private ArtistRepository artistRepository;
    private RestTemplate restTemplate;

    @Autowired
    public ArtistServiceImpl(ArtistRepository artistRepository,
                             RestTemplate restTemplate) {
        this.artistRepository = artistRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public void uploadArtists(String url) {
        List<Artist> artists = fetchArtistFromUrl(url);
        artistRepository.saveAll(artists);
    }

    @Override
    public Artist createArtist(Artist artist) {
        throwIfArtistExists(artist.getId());
        throwIfArtistExists(artist.getName());
        artistRepository.save(artist);
        return artist;
    }

    @Override
    public Artist updateArtist(Artist artist) {
        throwIfArtistDoesNotExists(artist.getId());
        artistRepository.save(artist);
        return artist;
    }

    @Override
    public void deleteArtist(int id) {
        throwIfArtistDoesNotExists(id);
        artistRepository.deleteById(id);
    }

    @Override
    public List<Artist> getArtists() {
        List<Artist> artists = new ArrayList<>();
        artistRepository.findAll().forEach(artists::add);
        if (artists.isEmpty()) {
            throw new EntityNotFoundException(NO_ARTISTS_EXCEPTION_MESSAGE);
        }
        return artists;
    }

    @Override
    public Artist getArtist(int id) {
        throwIfArtistDoesNotExists(id);
        return artistRepository.findById(id).get();
    }

    @Override
    public Artist getArtist(String name) {
        throwIfArtistDoesNotExists(name);
        return artistRepository.getArtistByName(name);
    }

    @Override
    public boolean checkIfArtistExists(int id) {
        return artistRepository.existsById(id);
    }

    @Override
    public boolean checkIfArtistExists(String name) {
        Artist artist = artistRepository.getArtistByName(name);
        return artist != null;
    }

    private List<Artist> fetchArtistFromUrl(String url) {
        String response = restTemplate.getForObject(url, String.class);
        JSONObject jsonObject = new JSONObject(response);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Artist> artists = new ArrayList<>();
        try {
            artists = objectMapper
                    .readValue(jsonObject.get("data").toString(), new TypeReference<List<Artist>>() {
                    });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return artists;
    }

    private void throwIfArtistDoesNotExists(int id) {
        if (!checkIfArtistExists(id)) {
            throw new EntityNotFoundException(
                    String.format(ARTIST_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfArtistDoesNotExists(String name) {
        if (!checkIfArtistExists(name)) {
            throw new EntityNotFoundException(
                    String.format(ARTIST_WITH_NAME_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, name));
        }
    }

    private void throwIfArtistExists(int id) {
        if (checkIfArtistExists(id)) {
            throw new DuplicateEntityException(
                    String.format(ARTIST_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfArtistExists(String name) {
        if (checkIfArtistExists(name)) {
            throw new DuplicateEntityException(
                    String.format(ARTIST_WITH_NAME_ALREADY_EXISTS_EXCEPTION_MESSAGE, name));
        }
    }
}