package com.telerik.ridepal.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/api.*"))
                .build()
                .apiInfo(apiEndPointsInfo());
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Spring Boot REST API")
                .description("RidePal Playlist Generator enables users to generate playlists for specific travel " +
                        "duration periods based on their preferred genres.\n" +
                        " \n" +
                        "A playlist consists of a list of individual tracks (songs). Each track has an artist, " +
                        "title, album, duration (playtime length) and rank (a numeric value). \n" +
                        "Each playlist has a user given title, associated tags (e.g. musical genres), a list of " +
                        "tracks with the associated track details, total playtime (the sum of the playtimes of " +
                        "all tracks in that playlist) and rank (the average of the ranks of all tracks in that " +
                        "playlist). ")
                .contact(new Contact("Anatoli Manolov, Evgeni Minkov",
                        "localhost:8080/about",
                        "manolov.anatoli@gmail.com"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}
