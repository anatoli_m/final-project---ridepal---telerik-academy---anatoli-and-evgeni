


$(document).ready(
    function () {
        let selected = 0;
        let selectedGenres = [];
        let selectedSliders = [];


        $("#rangeHipHop, #rangePop, #rangeMetal, #rangeHouse").change(function () {
            if(parseInt($("#rangeHipHop").val()) +
                parseInt($("#rangePop").val()) +
                parseInt($("#rangeMetal").val()) +
                parseInt($("#rangeHouse").val()) !== 100) {

                $("#btn-saveSliders").text("Total percentage must equals to 100!");
                $("#btn-saveSliders").prop("disabled", true);
            } else {
                $("#btn-saveSliders").text("Save").attr("disable", "false");
                $("#btn-saveSliders").prop("disabled", false);
            }
        });

        //Hip-hop select button
        $("#chooseHipHop, #choosePop, #chooseMetal, #chooseHouse").click(function () {
            if ($(this).text() === "Choose") {
                $(this).text("Selected");
                $(this).attr('style', "background-color: white; color: green");
                $("#" + "slider" + String($(this).attr("id")).substr(6)).removeAttr("hidden");
                selected += 1;
                selectedGenres.push($(this).attr("id"));
                if (selected === 1) {
                    $("#btn-overview").show();
                    selectedGenres.forEach(genre => {
                        let genreName = "#" + genre;
                        $(genreName).text("Selected " + 100 + "%")
                    });
                    $("#btn-updateSliders").hide();
                    selectedSliders.push("range" + String($(this).attr("id")).substr(6));
                    selectedSliders.forEach(slide => {
                        let slideName = "#" + slide;
                        let rangeSlideName = "#" + slide + "Value";
                        $(slideName).val(100);
                        $(rangeSlideName).text(100);
                    });

                } else if (selected === 2) {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").show();
                    selectedGenres.forEach(genre => {
                        let genreName = "#" + genre;
                        $(genreName).text("Selected " + 50 + "%")
                    });
                    selectedSliders.push("range" + String($(this).attr("id")).substr(6));
                    selectedSliders.forEach(slide => {
                        let slideName = "#" + slide;
                        let rangeSlideName = "#" + slide + "Value";
                        $(slideName).val(50);
                        $(rangeSlideName).text(50);
                    });

                } else if (selected === 3) {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").show();
                    for (let i = 0; i < 2; i++) {
                        let genreName = "#" + selectedGenres[i];
                        $(genreName).text("Selected " + 33 + "%")
                    }
                    let genreName = "#" + selectedGenres[2];
                    $(genreName).text("Selected " + 34 + "%");

                    selectedSliders.push("range" + String($(this).attr("id")).substr(6));

                    for (let i = 0; i < 2; i++) {
                        let slideName = "#" + selectedSliders[i];
                        let rangeSlideName = "#" + selectedSliders[i] + "Value";
                        $(slideName).val(33);
                        $(rangeSlideName).text(33);
                    }
                    let slideName = "#" + selectedSliders[2];
                    let rangeSlideName = "#" + selectedSliders[2] + "Value";
                    $(slideName).val(34);
                    $(rangeSlideName).text(34);

                } else if (selected === 4) {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").show();
                    selectedGenres.forEach(genre => {
                        let genreName = "#" + genre;
                        $(genreName).text("Selected " + 25 + "%")
                    });

                    selectedSliders.push("range" + String($(this).attr("id")).substr(6));
                    selectedSliders.forEach(slide => {
                        let slideName = "#" + slide;
                        let rangeSlideName = "#" + slide + "Value";
                        $(slideName).val(25);
                        $(rangeSlideName).text(25);
                    });
                } else {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").hide();
                }

            } else {
                $(this).text("Choose");
                $(this).attr('style', "background-color: #00000000; color: #f8f9fa");
                $("#" + "slider" + String($(this).attr("id")).substr(6)).prop("hidden","hidden");

                let index = selectedGenres.indexOf($(this).attr("id"));
                if (index !== -1) selectedGenres.splice(index, 1);

                let currentSlide = "range" + String($(this).attr("id")).substr(6);
                let indexSlide = selectedSliders.indexOf(currentSlide);
                if (indexSlide !== -1) selectedSliders.splice(indexSlide, 1);
                currentSlide = "#" + currentSlide;
                let currentSlideName = currentSlide + "Value";
                $(currentSlide).val(0);
                $(currentSlideName).text(0);

                selected -= 1;
                if (selected === 1) {
                    $("#btn-overview").show();
                    $("#btn-updateSliders").hide();
                    selectedGenres.forEach(genre => {
                        let genreName = "#" + genre;
                        $(genreName).text("Selected " + 100 + "%")
                    });
                    selectedSliders.forEach(slide => {
                        let slideName = "#" + slide;
                        let currentSlideName = slideName + "Value";
                        $(slideName).val(100);
                        $(currentSlideName).text(100);
                    });
                } else if (selected === 2) {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").show();
                    selectedGenres.forEach(genre => {
                        let genreName = "#" + genre;
                        $(genreName).text("Selected " + 50 + "%")
                    });
                    selectedSliders.forEach(slide => {
                        let slideName = "#" + slide;
                        let currentSlideName = slideName + "Value";
                        $(slideName).val(50);
                        $(currentSlideName).text(50);
                    });
                } else if (selected === 3) {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").show();
                    for (let i = 0; i < 2; i++) {
                        let genreName = "#" + selectedGenres[i];
                        $(genreName).text("Selected " + 33 + "%")
                    }
                    let genreName = "#" + selectedGenres[2];
                    $(genreName).text("Selected " + 34 + "%");

                    for (let j = 0; j < 2; j++) {
                        let slideName = "#" + selectedSliders[j];
                        let currentSlideName = "#" + selectedSliders[j] + "Value";
                        $(slideName).val(33);
                        $(currentSlideName).text(33);
                    }
                    let slideName = "#" + selectedSliders[2];
                    let currentSlideName = "#" + selectedSliders[2] + "Value";
                    $(slideName).val(34);
                    $(currentSlideName).text(34);

                } else {
                    $("#btn-overview").hide();
                    $("#btn-updateSliders").hide();
                }
            }
        });


        $("#btn-saveSliders").click(function () {
            $("#btn-updateSliders").hide();
            $("#btn-overview").show();

            if(parseInt($("#rangeHipHop").val()) === 0) {
                $('#chooseHipHop').text("Choose");
                $('#chooseHipHop').attr('style', "background-color: #00000000; color: #f8f9fa");

                let index = selectedGenres.indexOf($('#chooseHipHop').attr("id"));
                if (index !== -1) {
                    selectedGenres.splice(index, 1);
                    selected-=1;
                }

                let currentSlide = "range" + String($('#chooseHipHop').attr("id")).substr(6);
                let indexSlide = selectedSliders.indexOf(currentSlide);
                if (indexSlide !== -1) selectedSliders.splice(indexSlide, 1);


            } else {
                $('#chooseHipHop').text("Selected " + $("#rangeHipHop").val() + "%");
            }

            if(parseInt($("#rangePop").val()) === 0) {
                $('#choosePop').text("Choose");
                $('#choosePop').attr('style', "background-color: #00000000; color: #f8f9fa");

                let index = selectedGenres.indexOf($('#choosePop').attr("id"));
                if (index !== -1) {
                    selectedGenres.splice(index, 1);
                    selected-=1;
                }

                let currentSlide = "range" + String($('#choosePop').attr("id")).substr(6);
                let indexSlide = selectedSliders.indexOf(currentSlide);
                if (indexSlide !== -1) selectedSliders.splice(indexSlide, 1);


            } else {
                $('#choosePop').text("Selected " + $("#rangePop").val() + "%");
            }

            if(parseInt($("#rangeMetal").val()) === 0) {
                $('#chooseMetal').text("Choose");
                $('#chooseMetal').attr('style', "background-color: #00000000; color: #f8f9fa");

                let index = selectedGenres.indexOf($('#chooseMetal').attr("id"));
                if (index !== -1) {
                    selectedGenres.splice(index, 1);
                    selected-=1;
                }

                let currentSlide = "range" + String($('#chooseMetal').attr("id")).substr(6);
                let indexSlide = selectedSliders.indexOf(currentSlide);
                if (indexSlide !== -1) selectedSliders.splice(indexSlide, 1);


            } else {
                $('#chooseMetal').text("Selected " + $("#rangeMetal").val() + "%");
            }

            if(parseInt($("#rangeHouse").val()) === 0) {
                $('#chooseHouse').text("Choose");
                $('#chooseHouse').attr('style', "background-color: #00000000; color: #f8f9fa");

                let index = selectedGenres.indexOf($('#chooseHouse').attr("id"));
                if (index !== -1) {
                    selectedGenres.splice(index, 1);
                    selected-=1;
                }

                let currentSlide = "range" + String($('#chooseHouse').attr("id")).substr(6);
                let indexSlide = selectedSliders.indexOf(currentSlide);
                if (indexSlide !== -1) selectedSliders.splice(indexSlide, 1);

            } else {
                $('#chooseHouse').text("Selected " + $("#rangeHouse").val() + "%");
            }
        });
    });

