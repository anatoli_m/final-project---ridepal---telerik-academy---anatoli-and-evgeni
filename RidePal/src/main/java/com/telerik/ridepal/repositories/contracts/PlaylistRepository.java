package com.telerik.ridepal.repositories.contracts;

import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.Track;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;

public interface PlaylistRepository extends CrudRepository<Playlist, Serializable> {

    @Query(value = "SELECT * FROM ridepaldb.playlists WHERE user_id = ?1 AND enabled <> 0", nativeQuery = true)
    List<Playlist> getPlaylistsByUser(int userId);

    @Query(value = "SELECT * FROM ridepaldb.playlists WHERE playlist_id = ?1 AND enabled <> 0", nativeQuery = true)
    Playlist getPlaylistsById(int id);

    @Query(value = "SELECT * FROM ridepaldb.playlist_tracks WHERE playlist_id = ?1", nativeQuery = true)
    List<Track> getTracks(int playlistId);

    @Query(value = "SELECT * FROM ridepaldb.playlists p WHERE p.title = ?1 AND enabled <> 0", nativeQuery = true)
    List<Playlist> getPlaylistsByTitle(String title);

    @Query(value = "SELECT * FROM ridepaldb.playlists p WHERE enabled <> 0 ORDER BY `rank` DESC LIMIT 3", nativeQuery = true)
    List<Playlist> getPTop3Playlists();

    @Query(value = "SELECT ridepaldb.playlists.* FROM ridepaldb.playlists \n" +
            "JOIN ridepaldb.saved_playlists ON (playlists.playlist_id = saved_playlists.playlist_id)\n" +
            "WHERE saved_playlists.user_id = ?1 AND enabled <> 0", nativeQuery = true)
    List<Playlist> getSavedPlaylists(int userId);

    @Query(value = "SELECT ridepaldb.playlists.* FROM ridepaldb.playlists " +
            "WHERE ridepaldb.playlists.private = FALSE AND playlists.user_id <> ?1 AND enabled <> 0 AND private <>1",
            nativeQuery = true)
    List<Playlist> getPublicPlaylists(int userId);
}
