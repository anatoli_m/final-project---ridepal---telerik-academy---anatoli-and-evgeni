package com.telerik.ridepal.models.DTO;

import com.telerik.ridepal.validation.PasswordMatches;
import com.telerik.ridepal.validation.ValidName;
import com.telerik.ridepal.validation.ValidPassword;
import com.telerik.ridepal.validation.ValidUsername;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@PasswordMatches
public class UserDto {

    @ValidName(message = "Username can contain letters only and should be at least 4 characters")
    private String firstName;

    @ValidName(message = "Username can contain letters only and should be at least 4 characters")
    private String lastName;

    @ValidUsername(message = "Username can contain letters and numbers only and should be at least 4 characters")
    private String username;

    @Email(message = "This is not a valid email address")
    @NotNull
    @NotEmpty(message = "Email cannot be empty")
    private String email;

    @NotNull
    @NotEmpty(message = "Password cannot be empty")
    @Size(min = 4, max = 50, message = "Password must be at lest 4 symbols")
    @ValidPassword(message = "Invalid password format")
    private String password;

    @NotNull
    @NotEmpty(message = "Password cannot be empty")
    @Size(min = 4, max = 50, message = "Password must be at lest 4 symbols")
    @ValidPassword(message = "Password must contain at least one upper case, one lower case and a number")
    private String matchingPassword;
}
