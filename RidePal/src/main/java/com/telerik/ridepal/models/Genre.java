package com.telerik.ridepal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "genres")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Genre {

    @Id
    @Column(name = "genre_id")
    private int id;

    @NotNull
    @Size(min = 3, max = 20)
    @Column(name = "genre")
    private String name;

    @Column(name = "picture")
    private String picture_big;

    @Column(name = "enabled")
    private boolean enabled = true;

    @JsonIgnore
    @ManyToMany(mappedBy = "genres")
    private List<Playlist> playlists = new ArrayList<>();

    @Override
    public String toString() {
        return String.format("%s", getName());
    }

}
