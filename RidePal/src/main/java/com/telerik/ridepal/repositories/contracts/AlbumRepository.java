package com.telerik.ridepal.repositories.contracts;

import com.telerik.ridepal.models.Album;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;

public interface AlbumRepository extends CrudRepository<Album, Serializable> {

    @Query("SELECT а FROM Album а WHERE а.title = ?1")
    Album getAlbumByTitle(String title);

    @Query("SELECT a FROM Album a WHERE a.artist_id = ?1")
    List<Album> getAlbumsByArtist(int artistId);
}
