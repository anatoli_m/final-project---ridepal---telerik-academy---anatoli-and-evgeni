package com.telerik.ridepal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.*;

@ToString
@NoArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode
@Table(name = "tracks")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Track {

    @Id
    @Column(name = "track_id")
    private int id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "link")
    private String link;

    @NotNull
    @PositiveOrZero
    @Column(name = "duration")
    private int duration;

    @NotNull
    @PositiveOrZero
    @Column(name = "rank")
    private int rank;

    @Column(name = "preview")
    private String preview;

    @Column(name = "artist_id")
    private int artistId;

    @Column(name = "album_id")
    private int albumId;

    @Column(name = "enabled")
    @JsonIgnore
    private boolean enabled = true;

    @JsonIgnore
    @ManyToMany(mappedBy = "tracks")
    private List<Playlist> playlists = new ArrayList<>();

    @JsonProperty("artist")
    private void unpackNestedArtistId(Map<String, Object> artist) {
        this.artistId = (int) artist.get("id");
    }

    @JsonProperty("album")
    private void unpackNestedAlbumId(Map<String, Object> album) {
        this.albumId = (int) album.get("id");
    }
}
