package com.telerik.ridepal.controllers;

import com.telerik.ridepal.services.contracts.DatabaseUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/updateDB")
public class DatabaseUpdateController {

    private DatabaseUpdateService databaseUpdateService;

    @Autowired
    public DatabaseUpdateController(DatabaseUpdateService databaseUpdateService) {
        this.databaseUpdateService = databaseUpdateService;
    }

    @GetMapping
    public void initializeDatabase() {
        databaseUpdateService.updateDatabase();
    }
}
