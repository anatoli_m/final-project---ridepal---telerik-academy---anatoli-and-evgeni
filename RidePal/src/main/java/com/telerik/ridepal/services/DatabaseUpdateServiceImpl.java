package com.telerik.ridepal.services;

import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.models.Genre;
import com.telerik.ridepal.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseUpdateServiceImpl implements DatabaseUpdateService {

    private static final String GENRES_URL = "https://api.deezer.com/genre";
    private static final String ARTISTS_FROM_GENRE_URL = "https://api.deezer.com/genre/%d/artists";
    private static final String ARTIST_ALBUMS_URL = "https://api.deezer.com/artist/%d/albums";
    private static final String ALBUM_TRACKS_URL = "https://api.deezer.com/album/%d/tracks";
    private static final String[] genresToFetch = {"Pop", "Jazz", "Rap/Hip Hop", "Metal"};

    private GenreService genreService;
    private ArtistService artistService;
    private AlbumService albumService;
    private TrackService trackService;

    @Autowired
    public DatabaseUpdateServiceImpl(GenreService genreService,
                                     ArtistService artistService,
                                     AlbumService albumService,
                                     TrackService trackService) {
        this.genreService = genreService;
        this.artistService = artistService;
        this.albumService = albumService;
        this.trackService = trackService;
    }

    public void updateDatabase() {
        updateGenres();
        updateArtists();
        updateAlbums();
        updateTracks();
    }

    public void updateGenres() {
        Genre genre = new Genre();
        genre.setId(-1);
        genre.setName("Unknown");
        genre.setPicture_big("https://e-cdns-images.dzcdn.net/images/misc//500x500-000000-80-0-0.jpg");
        genre.setEnabled(true);
        genreService.createGenre(genre);
        genreService.uploadGenres(GENRES_URL);
    }

    public void updateArtists() {
        for (String toFetch : genresToFetch) {
            Genre genre = genreService.getGenreByName(toFetch);
            if (genre != null) {
                String artistURL = String.format(ARTISTS_FROM_GENRE_URL, genre.getId());
                artistService.uploadArtists(artistURL);
            }
        }
    }

    public void updateAlbums() {
        List<Artist> artists = artistService.getArtists();
        for (Artist artist : artists) {
            String albumsURL = String.format(ARTIST_ALBUMS_URL, artist.getId());
            albumService.uploadArtistAlbums(albumsURL, artist.getId());
        }
    }

    public void updateTracks() {
        albumService.getAlbums().forEach(album -> {
            String tracksURL = String.format(ALBUM_TRACKS_URL, album.getId());
            trackService.uploadTracksFromAlbum(tracksURL, album);
        });
    }
}
