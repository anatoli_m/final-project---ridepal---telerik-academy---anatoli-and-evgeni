package com.telerik.ridepal.controllers.rest;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/playlists")
public class PlaylistsRestController {

    private PlaylistService playlistService;

    @Autowired
    public PlaylistsRestController(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @GetMapping
    public List<Playlist> getAllPlaylists() {
        List<Playlist> playlists = new ArrayList<>();
        try {
            playlists = playlistService.getPlaylists();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return playlists;
    }

    @GetMapping("/{id}")
    public Playlist getPlaylist(@PathVariable int id) {
        Playlist playlist = new Playlist();
        try {
            playlist = playlistService.getPlaylistById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return playlist;
    }

    @PostMapping
    public Playlist createPlaylist(@RequestBody Playlist playlist) {
        try {
            playlistService.createPlaylist(playlist);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return playlist;
    }

    @PutMapping
    public Playlist updatePlaylist(@RequestBody Playlist playlist) {
        try {
            playlistService.updatePlaylist(playlist);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return playlist;
    }

    @DeleteMapping("/{id}")
    public void deletePlaylist(@PathVariable int id) {
        try {
            playlistService.deletePlaylist(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
