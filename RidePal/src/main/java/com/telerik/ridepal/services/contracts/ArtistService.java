package com.telerik.ridepal.services.contracts;

import com.telerik.ridepal.models.Artist;

import java.util.List;

public interface ArtistService {

    void uploadArtists(String url);

    Artist createArtist(Artist artist);

    Artist updateArtist(Artist artist);

    void deleteArtist(int id);

    List<Artist> getArtists();

    Artist getArtist(int id);

    Artist getArtist(String name);

    boolean checkIfArtistExists(int id);

    boolean checkIfArtistExists(String name);
}
