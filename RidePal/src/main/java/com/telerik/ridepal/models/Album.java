package com.telerik.ridepal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "albums")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {

    @Id
    @Column(name = "album_id")
    private int id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "tracklist")
    private String tracklist;

    @Column(name = "cover")
    private String cover_big;

    @NotNull
    @Column(name = "genre_id")
    private int genre_id;

    @NotNull
    @Column(name = "artist_id")
    private int artist_id;

    @Column(name = "release_date")
    private String release_date;

    @JsonIgnore
    @Column(name = "enabled")
    private boolean enabled = true;

}
