package com.telerik.ridepal.controllers.rest;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Track;
import com.telerik.ridepal.services.contracts.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tracks")
public class TracksRestController {

    TrackService trackService;

    @Autowired
    public TracksRestController(TrackService trackService) {
        this.trackService = trackService;
    }

    @PutMapping
    public void uploadTracks(@RequestBody List<Track> tracks){
        trackService.uploadTracks(tracks);
    }

    @GetMapping("/{title}")
    public List<Track> getTrackByTitle(@PathVariable String title) {
        List<Track> tracks = new ArrayList<>();
        try {
            tracks = trackService.getTracksByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return tracks;
    }

    @PostMapping
    public Track createTrack(@RequestBody Track track) {
        try {
            trackService.createTrack(track);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return track;
    }

    @PutMapping("/{id}")
    public Track updateTrack(@PathVariable int id, @RequestBody Track track) {
        try {
            trackService.updateTrack(track);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return track;
    }

    @DeleteMapping("/{id}")
    public void deleteTrack(@PathVariable int id) {
        try {
            trackService.deleteTrack(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
