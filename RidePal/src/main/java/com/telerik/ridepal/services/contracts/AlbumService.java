package com.telerik.ridepal.services.contracts;

import com.telerik.ridepal.models.Album;

import java.util.List;

public interface AlbumService {

    Album createAlbum(Album album);

    Album updateAlbum(Album album);

    void deleteAlbum(int id);

    List<Album> getAlbums();

    Album getAlbum(int id);

    Album getAlbum(String title);

    List<Album> getAlbumsByArtist(int id);

    void uploadArtistAlbums(String url, int artistId);

    boolean checkIfAlbumExists(int id);

    boolean checkIfAlbumExists(String title);
}
