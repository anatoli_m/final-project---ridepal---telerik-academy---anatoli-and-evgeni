package com.telerik.ridepal.models.DTO;

import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.Role;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.services.contracts.PlaylistService;
import com.telerik.ridepal.services.contracts.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DtoMapper {

    private PlaylistService playlistService;
    private TrackService trackService;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public DtoMapper(PlaylistService playlistService,
                     TrackService trackService,
                     BCryptPasswordEncoder passwordEncoder)
    {
        this.playlistService = playlistService;
        this.trackService = trackService;
        this.passwordEncoder = passwordEncoder;
    }

    public Playlist playlistFromDto(PlaylistDTO playlistDTO) {

        Playlist playlist = new Playlist();
        playlist.setTitle(playlistDTO.getTitle());
        playlist.setDescription(playlistDTO.getDescription());

        return playlist;
    }

    public User userFromDto(UserDto userDto) {
        Role role = new Role();
        role.setRole_id(2);
        role.setRole("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        User newUser = new User();
        newUser.setUsername(userDto.getUsername());
        newUser.setFirstName(userDto.getFirstName());
        newUser.setLastName(userDto.getLastName());
        newUser.setEmail(userDto.getEmail());
        newUser.setPassword(passwordEncoder.encode(userDto.getPassword()));
        newUser.setUserRoles(roles);
        newUser.setPicture(null);
        newUser.setEnabled(true);

        return newUser;
    }
}
