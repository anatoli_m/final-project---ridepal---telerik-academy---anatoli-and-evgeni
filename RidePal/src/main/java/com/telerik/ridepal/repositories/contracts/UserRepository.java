package com.telerik.ridepal.repositories.contracts;

import com.telerik.ridepal.models.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

public interface UserRepository extends CrudRepository<User, Serializable> {

    @Query("SELECT u FROM User u WHERE u.username = ?1")
    User getUserByUsername(String username);

    @Query("SELECT u FROM User u WHERE u.email = ?1")
    User getUserByEmail(String email);

    //Soft delete.
    @Transactional
    @Query("UPDATE User u SET u.enabled = FALSE WHERE u.user_id = ?1")
    @Modifying
    void deleteUser(int id);
}
