package com.telerik.ridepal.services.contracts;

import org.json.JSONException;

import java.util.List;

public interface LocationService {

    double getTravelDuration(String from, String to) throws JSONException;

    List<Double> getCentroid(String from, String to) throws JSONException;

}

