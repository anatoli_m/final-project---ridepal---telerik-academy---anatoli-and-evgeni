package com.telerik.ridepal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "playlists")
@SQLDelete(sql = "UPDATE playlists SET enabled = false WHERE playlist_id = ?")
@Where(clause = "enabled <> 0")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "generatePlaylist",
                procedureName = "generate_playlist",
                resultClasses = {Track.class},
                parameters = {
                        @StoredProcedureParameter(
                                name = "genre_name",
                                type = String.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "travel_duration",
                                type = Integer.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "include_remixes",
                                type = Boolean.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "include_live_performances",
                                type = Boolean.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "top_tracks_only",
                                type = Boolean.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "allow_same_artist",
                                type = Boolean.class,
                                mode = ParameterMode.IN)})
})

public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "playlist_id")
    private int playlist_id;


    @Column(name = "title")
    private String title;


    @Column(name = "description")
    private String description;


    @PositiveOrZero
    @Column(name = "duration")
    private int duration;

    @PositiveOrZero
    @Column(name = "rank")
    private int rank;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "cover")
    private String cover;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "playlist_tracks",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "track_id")
    )
    private List<Track> tracks = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "playlist_genres",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id")
    )
    private Set<Genre> genres = new HashSet<>();


    @JsonIgnore
    @Column(name = "enabled")
    private boolean enabled = true;

    @JsonIgnore
    @Column(name = "private")
    private boolean privateOn = true;

}
