package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.Track;
import com.telerik.ridepal.repositories.contracts.PlaylistRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.parameters.P;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.ridepal.Factory.createPlaylist;
import static com.telerik.ridepal.Factory.createTrack;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class PlaylistServiceImplTests {

    @InjectMocks
    PlaylistServiceImpl playlistService;

    @Mock
    PlaylistRepository playlistRepository;

    @Test
    public void createPlaylistShould_CallRepository() {
        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(false);

        //Act
        playlistService.createPlaylist(playlist);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void createPlaylistShould_CreatePlaylist() {
        //Arrange
        Playlist expectedPlaylist = createPlaylist();
        Mockito.when(playlistRepository.existsById(expectedPlaylist.getPlaylist_id())).thenReturn(false);

        //Act
        Playlist actualPlaylist = playlistService.createPlaylist(expectedPlaylist);

        //Assert
        Assert.assertSame(expectedPlaylist, actualPlaylist);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createPlaylistShould_Throw_WhenPlaylistIdAlreadyExists() {
        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(true);

        //Act
        playlistService.createPlaylist(playlist);
    }

    @Test
    public void updatePlaylistShould_CallRepository() {
        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(true);

        //Act
        playlistService.updatePlaylist(playlist);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void updatePlaylistShould_UpdatePlaylist() {
        //Arrange
        Playlist expectedPlaylist = createPlaylist();
        Mockito.when(playlistRepository.existsById(expectedPlaylist.getPlaylist_id())).thenReturn(true);

        //Act
        Playlist actualPlaylist = playlistService.updatePlaylist(expectedPlaylist);

        //Assert
        Assert.assertSame(expectedPlaylist, actualPlaylist);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updatePlaylistShould_Throw_WhenPlaylistDoesNotExist() {
        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(false);

        //Act
        Playlist actualPlaylist = playlistService.updatePlaylist(playlist);
    }

    @Test
    public void deletePlaylistShould_CallRepository() {
        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(true);

        //Act
        playlistService.deletePlaylist(playlist.getPlaylist_id());

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1))
                .deleteById(playlist.getPlaylist_id());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deletePlaylistShould_Throw_WhenPlaylistDoesNotExist() {
        //Arrange
        Mockito.when(playlistRepository.existsById(anyInt())).thenReturn(false);

        //Act
        playlistService.deletePlaylist(anyInt());
    }

    @Test
    public void generateCoverForPlaylistShould_GenerateCover() {
        //Arrange
        String cover = "";

        //Act
        String returnedCover = playlistService.GenerateCoverForPlaylist();

        //Assert
        Assert.assertNotEquals(cover, returnedCover);
    }

    @Test
    public void getPlaylistsShould_CallRepository() {
        //Arrange
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(createPlaylist());
        Mockito.when(playlistRepository.findAll()).thenReturn(playlists);

        //Act
        playlistService.getPlaylists();

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getPlaylistsShould_ReturnPlaylists() {
        //Arrange
        List<Playlist> expectedPlaylists = new ArrayList<>();
        expectedPlaylists.add(createPlaylist());
        Mockito.when(playlistRepository.findAll()).thenReturn(expectedPlaylists);

        //Act
        List<Playlist> actualPlaylists = playlistService.getPlaylists();

        //Assert
        Assert.assertSame(expectedPlaylists.size(), actualPlaylists.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPlaylistByIdShould_Throw_WhenPlaylistDoesNotExist() {
        //Arrange
        Mockito.when(playlistRepository.existsById(anyInt())).thenReturn(false);

        //Act
        playlistService.getPlaylistById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPlaylistByTitleShould_Throw_WhenPlaylistDoesNotExist() {
        //Arrange
        Mockito.when(playlistRepository.getPlaylistsByTitle(anyString())).thenReturn(null);

        //Act
        playlistService.getPlaylistByTitle(anyString());
    }

    @Test
    public void getPlaylistByTitleShould_CallRepository() {
        //Arrange
        Playlist playlist = createPlaylist();
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(playlist);
        Mockito.when(playlistRepository.getPlaylistsByTitle(playlist.getTitle()))
                .thenReturn(playlists);

        //Act
        playlistService.getPlaylistByTitle(playlist.getTitle());

        //Arrange
        Mockito.verify(playlistRepository, Mockito.times(2))
                .getPlaylistsByTitle(playlist.getTitle());
    }

    @Test
    public void getPlaylistByTitleShould_ReturnPlaylist() {
        //Arrange
        Playlist playlist = createPlaylist();
        List<Playlist> expectedPlaylists = new ArrayList<>();
        expectedPlaylists.add(playlist);
        Mockito.when(playlistRepository.getPlaylistsByTitle(anyString()))
                .thenReturn(expectedPlaylists);

        //Act
        List<Playlist> actualPlaylists = playlistService.getPlaylistByTitle(anyString());

        //Arrange
        Assert.assertSame(expectedPlaylists.size(), actualPlaylists.size());
    }

    @Test
    public void getPlaylistsByUserShould_CallRepository() {
        //Arrange
        List<Playlist> playlists = new ArrayList<>();
        Playlist playlist = createPlaylist();
        playlists.add(playlist);
        Mockito.when(playlistRepository.getPlaylistsByUser(playlist.getUser().getUser_id()))
                .thenReturn(playlists);

        //Act
        playlistService.getPlaylistByUser(playlist.getUser().getUser_id());

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1))
                .getPlaylistsByUser(playlist.getUser().getUser_id());
    }

    @Test
    public void getPlaylistsByUserShould_ReturnPlaylists() {
        //Arrange
        List<Playlist> expectedPlaylists = new ArrayList<>();
        Playlist playlist = createPlaylist();
        expectedPlaylists.add(playlist);
        Mockito.when(playlistRepository.getPlaylistsByUser(playlist.getUser().getUser_id()))
                .thenReturn(expectedPlaylists);

        //Act
        List<Playlist> actualPlaylists =
                playlistService.getPlaylistByUser(playlist.getUser().getUser_id());

        //Assert
        Assert.assertSame(expectedPlaylists.size(), actualPlaylists.size());
    }

    @Test
    public void getTracksShould_ReturnPlaylistTracks() {
        //Arrange
        Playlist playlist = createPlaylist();
        List<Track> expectedTracks = new ArrayList<>();
        expectedTracks.add(createTrack());
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(true);
        Mockito.when(playlistRepository.getTracks(playlist.getPlaylist_id())).thenReturn(expectedTracks);

        //Act
        List<Track> actualTracks = playlistService.getTracks(playlist.getPlaylist_id());

        //Arrange
        Assert.assertSame(expectedTracks, actualTracks);
    }

    @Test
    public void getTracksShould_CallRepository() {
        //Arrange
        Playlist playlist = createPlaylist();
        List<Track> tracks = new ArrayList<>();
        tracks.add(createTrack());
        Mockito.when(playlistRepository.existsById(playlist.getPlaylist_id())).thenReturn(true);
        Mockito.when(playlistRepository.getTracks(playlist.getPlaylist_id())).thenReturn(tracks);

        //Act
        playlistService.getTracks(playlist.getPlaylist_id());

        //Arrange
        Mockito.verify(playlistRepository, Mockito.times(1))
                .getTracks(playlist.getPlaylist_id());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTracksShould_Throw_WhenPlaylistDoesNotExist() {
        //Arrange
        Mockito.when(playlistRepository.existsById(anyInt())).thenReturn(false);

        //Act
        playlistService.getTracks(anyInt());
    }

    @Test
    public void checkIfPlaylistExistsIdShould_CallRepository() {
        //Act
        playlistService.checkIfPlaylistExists(anyInt());

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).existsById(anyInt());
    }

    @Test
    public void checkIfPlaylistExistsTitleShould_CallRepository() {
        //Act
        playlistService.checkIfPlaylistExists(anyString());

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1))
                .getPlaylistsByTitle(anyString());
    }

    @Test
    public void getPublicPlaylistsShould_CallRepository() {
        //Arrange
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(createPlaylist());
        Mockito.when(playlistRepository.getPublicPlaylists(anyInt())).thenReturn(playlists);

        //Act
        playlistService.getPublicPlaylists(anyInt());

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1))
                .getPublicPlaylists(anyInt());
    }

    @Test
    public void getPublicPlaylistsShould_ReturnPlaylists() {
        //Arrange
        List<Playlist> expectedPlaylists = new ArrayList<>();
        expectedPlaylists.add(createPlaylist());
        Mockito.when(playlistRepository.getPublicPlaylists(anyInt())).thenReturn(expectedPlaylists);

        //Act
        List<Playlist> actualPlaylists = playlistService.getPublicPlaylists(anyInt());

        //Assert
        Assert.assertSame(expectedPlaylists.size(), actualPlaylists.size());
    }

    @Test
    public void getUserSavedPlaylistsShould_CallRepository() {
        //Arrange
        List<Playlist> playlists = new ArrayList<>();
        playlists.add(createPlaylist());
        Mockito.when(playlistRepository.getSavedPlaylists(anyInt())).thenReturn(playlists);

        //Act
        playlistService.getUserSavedPlaylists(anyInt());

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1))
                .getSavedPlaylists(anyInt());
    }

    @Test
    public void getUserSavedPlaylistsShould_ReturnPlaylists() {
        //Arrange
        List<Playlist> expectedPlaylists = new ArrayList<>();
        expectedPlaylists.add(createPlaylist());
        Mockito.when(playlistRepository.getSavedPlaylists(anyInt())).thenReturn(expectedPlaylists);

        //Act
        List<Playlist> actualPlaylists = playlistService.getUserSavedPlaylists(anyInt());

        //Assert
        Assert.assertSame(expectedPlaylists.size(), actualPlaylists.size());
    }

    @Test
    public void getPlaylistsByRankShould_ReturnPlaylists() {
        //Arrange
        List<Playlist> expectedPlaylists = new ArrayList<>();
        expectedPlaylists.add(createPlaylist());
        Mockito.when(playlistRepository.findAll()).thenReturn(expectedPlaylists);

        //Act
        List<Playlist> actualPlaylists = playlistService
                .getPlaylistsByRank(expectedPlaylists.get(0).getRank());

        //Assert
        Assert.assertSame(expectedPlaylists.size(), actualPlaylists.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPlaylistsByRankShould_Throw_WhenNoPlaylists() {
        List<Playlist> expectedPlaylists = new ArrayList<>();
        Mockito.when(playlistRepository.findAll()).thenReturn(expectedPlaylists);

        //Act
        playlistService.getPlaylistsByRank(0);
    }
}
