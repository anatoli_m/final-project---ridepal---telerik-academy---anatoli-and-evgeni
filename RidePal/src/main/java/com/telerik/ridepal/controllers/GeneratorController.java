package com.telerik.ridepal.controllers;

import com.telerik.ridepal.models.*;
import com.telerik.ridepal.models.DTO.DtoMapper;
import com.telerik.ridepal.models.DTO.PlaylistDTO;
import com.telerik.ridepal.services.contracts.*;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

@Controller
public class GeneratorController {

    private LocationService locationService;
    private PlaylistService playlistService;
    private TrackService trackService;
    private GenreService genreService;
    private UserService userService;
    private DtoMapper dtoMapper;

    @Autowired
    public GeneratorController(LocationService locationService, PlaylistService playlistService, DtoMapper dtoMapper,
                               TrackService trackService, GenreService genreService, UserService userService) {
        this.locationService = locationService;
        this.playlistService = playlistService;
        this.trackService = trackService;
        this.genreService = genreService;
        this.userService = userService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/generate")
    public String generator(Model model, Principal principal) {
        model.addAttribute("form", new LocationForm());
        String url = "https://www.bing.com/api/maps/mapcontrol?key="
                + System.getenv("BingApiKey")
                + "&callback=loadMapScenario";

        if (principal != null) {
            User user = userService.getUser(principal.getName());
            model.addAttribute("user", user);
        }

        model.addAttribute("url", url);
        return "generate";
    }


    @PostMapping(value = "/generate")
    @ResponseBody
    public ResponseEntity<Playlist> getTripDuration(@RequestBody LocationForm form, Model model,
                                  BindingResult bindingResult, Principal principal) {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setTitle(form.getTitle());
        playlistDTO.setDescription(form.getDescription());
        Playlist newPlaylist = dtoMapper.playlistFromDto(playlistDTO);
        newPlaylist.setUser(userService.getUser(principal.getName()));
        double duration = 0;

        try {
            duration = locationService.getTravelDuration(form.getFromDestination(), form.getToDestination());
            newPlaylist.setDuration((int)duration);
        } catch (IllegalArgumentException ia) {

            model.addAttribute("error", "Can't find location!");
        } catch (JSONException e) {
          return new ResponseEntity<>(newPlaylist, HttpStatus.NOT_ACCEPTABLE);
        }

        HashMap<String, Integer> genres = new HashMap<>();
        genres.put("Rap/Hip Hop", form.getHiphop());
        genres.put("Pop", form.getPop());
        genres.put("Metal", form.getMetal());
        genres.put("Jazz", form.getHouse());

        Set<Genre> genresToAdd = new HashSet<>();

        for (Map.Entry<String, Integer> entry : genres.entrySet()) {
            if (entry.getValue() != 0) {
              genresToAdd.add(genreService.getGenreByName(entry.getKey()));
            }
        }

        newPlaylist.setGenres(genresToAdd);

        boolean distinctArtist = form.isDistinctArtists();
        boolean includeRemixes = form.isIncludeRemixes();
        boolean topTracks = form.isTopTracks();
        boolean includeLivePerformances = form.isIncludeLivePerformance();
        boolean makePublic = form.isMakePublic();

        List<Track> tracks2 = playlistService.generatePlaylist(genres, duration, distinctArtist, includeRemixes,
                includeLivePerformances, topTracks);

        newPlaylist.setTracks(tracks2);

        double avgRank =tracks2.stream()
                .mapToInt(Track::getRank)
                .average()
                .orElse(Double.NaN);

        newPlaylist.setRank((int) (avgRank));
        String cover = playlistService.GenerateCoverForPlaylist();
        newPlaylist.setCover(cover);
        newPlaylist.setPrivateOn(!makePublic);
        playlistService.createPlaylist(newPlaylist);


        return new ResponseEntity<>(newPlaylist, HttpStatus.OK);
    }

    @PostMapping(value = "/getLocation")
    @ResponseBody
    public ResponseEntity<LocationForm> getLocation(@RequestBody LocationForm form) {

        double duration = 0;
        List<Double> centroid = null;
        try {
            duration = locationService.getTravelDuration(form.getFromDestination(), form.getToDestination());
            centroid = locationService.getCentroid(form.getFromDestination(), form.getToDestination());
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(form, HttpStatus.NOT_ACCEPTABLE);
        }

        form.setCentroid(centroid);
        form.setDuration(duration);
        return new ResponseEntity<>(form, HttpStatus.OK);
    }


}
