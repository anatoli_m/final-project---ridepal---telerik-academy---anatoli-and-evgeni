package com.telerik.ridepal.models.DTO;

import com.telerik.ridepal.models.Track;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class PlaylistDTO {


    private int playlist_id;

    @NotNull
    @Size(min = 3, max = 50)
    private String title;

    @NotNull
    @Size(min = 3, max = 100)
    private String description;

    @NotNull
    @PositiveOrZero
    private int duration;

    @PositiveOrZero
    private int rank;

    private int userId;

    private String cover;

    private Set<Track> tracks = new HashSet<>();

    private Set<Track> genres = new HashSet<>();

}
