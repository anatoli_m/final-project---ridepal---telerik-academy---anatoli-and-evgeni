package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Album;
import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.repositories.contracts.AlbumRepository;
import com.telerik.ridepal.services.contracts.ArtistService;
import com.telerik.ridepal.services.contracts.GenreService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.ridepal.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AlbumServiceImplTests {

    @Mock
    AlbumRepository albumRepository;

    @Mock
    ArtistService artistService;

    @Mock
    GenreService genreService;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    AlbumServiceImpl albumService;

    @Test
    public void createAlbumShould_CallRepository() {
        //Arrange
        Album album = createAlbum();

        //Act
        albumService.createAlbum(album);

        //Assert
        Mockito.verify(albumRepository, times(1)).save(album);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createAlbumShould_Throw_WhenIdExists() {
        //Arrange
        Album album = createAlbum();
        Mockito.when(albumRepository.existsById(album.getId())).thenReturn(true);

        //Act
        albumService.createAlbum(album);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createAlbumShould_Throw_WhenTitleAlreadyExists() {
        //Arrange
        Album album = createAlbum();
        Mockito.when(albumRepository.getAlbumByTitle(album.getTitle()))
                .thenReturn(album);
        //Act
        albumService.createAlbum(album);
    }

    @Test
    public void createAlbumShould_CreateAlbum() {
        //Arrange
        Album expectedAlbum = createAlbum();
        Mockito.when(albumRepository.save(any(Album.class))).thenReturn(expectedAlbum);

        //Act
        Album actualAlbum = albumService.createAlbum(expectedAlbum);

        //Assert
        Assert.assertSame(expectedAlbum, actualAlbum);
    }

    @Test
    public void updateAlbumShould_CallRepository() {
        //Arrange
        Album album = createAlbum();
        Mockito.when(albumRepository.existsById(album.getId())).thenReturn(true);

        //Act
        albumService.updateAlbum(album);

        //Assert
        Mockito.verify(albumRepository, times(1)).save(album);
    }

    @Test
    public void updateAlbumShould_UpdateAlbum() {
        //Arrange
        Album expectedAlbum = createAlbum();
        Mockito.when(albumRepository.save(any(Album.class))).thenReturn(expectedAlbum);
        Mockito.when(albumRepository.existsById(expectedAlbum.getId())).thenReturn(true);

        //Act
        Album actualAlbum = albumService.updateAlbum(expectedAlbum);

        //Assert
        Assert.assertSame(actualAlbum, expectedAlbum);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateAlbumShould_Throw_WhenAlbumDoesNotExist() {
        //Act
        albumService.updateAlbum(createAlbum());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteAlbumShould_Throw_WhenAlbumDoesNotExist() {
        //Act
        albumService.deleteAlbum(anyInt());
    }

    @Test
    public void getAlbumsShould_CallRepository() {
        //Arrange
        List<Album> albums = new ArrayList<>();
        Album album = createAlbum();
        albums.add(album);
        Mockito.when(albumRepository.findAll()).thenReturn(albums);

        //Act
        albumService.getAlbums();

        //Assert
        Mockito.verify(albumRepository, times(1)).findAll();
    }

    @Test
    public void getAlbumsShould_ReturnAlbums() {
        //Arrange
        List<Album> expectedAlbums = new ArrayList<>();
        Album album = createAlbum();
        expectedAlbums.add(album);
        Mockito.when(albumRepository.findAll()).thenReturn(expectedAlbums);

        //Act
        List<Album> actualAlbums = albumService.getAlbums();

        //Assert
        Assert.assertSame(actualAlbums.size(), expectedAlbums.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumsShouldThrow_When_NoAlbums() {
        //Act
        albumService.getAlbums();
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumShould_Throw_WhenAlbumDoesNotExist() {

        albumService.getAlbum(anyInt());
    }

    @Test
    public void getAlbumShould_CallRepository() {
        //Arrange
        Album album = createAlbum();
        Mockito.when(albumRepository.getAlbumByTitle(album.getTitle())).thenReturn(album);

        //Act
        albumService.getAlbum(album.getTitle());

        //Assert
        Mockito.verify(albumRepository, times(2))
                .getAlbumByTitle(album.getTitle());
    }

    @Test
    public void getAlbumShould_ReturnAlbum() {
        //Arrange
        Album expectedAlbum = createAlbum();
        Mockito.when(albumRepository.getAlbumByTitle(expectedAlbum.getTitle())).thenReturn(expectedAlbum);

        //Act
        Album actualAlbum = albumService.getAlbum(expectedAlbum.getTitle());

        //Assert
        Assert.assertSame(actualAlbum, expectedAlbum);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumByTitleShould_Throw_WhenAlbumDoesNotExist() {

        albumService.getAlbum(anyString());
    }

    @Test
    public void getAlbumsByArtistShould_CallRepository() {
        //Arrange
        List<Album> albums = new ArrayList<>();
        Album album = createAlbum();
        albums.add(album);
        Mockito.when(artistService.checkIfArtistExists(anyInt())).thenReturn(true);
        Mockito.when(albumRepository.getAlbumsByArtist(anyInt())).thenReturn(albums);

        //Act
        albumService.getAlbumsByArtist(anyInt());

        //Assert
        Mockito.verify(albumRepository, times(1)).getAlbumsByArtist(anyInt());
    }

    @Test
    public void getAlbumsByArtistShould_ReturnAlbums() {
        //Arrange
        List<Album> expectedAlbums = new ArrayList<>();
        Album album = createAlbum();
        expectedAlbums.add(album);
        Mockito.when(artistService.checkIfArtistExists(anyInt())).thenReturn(true);
        Mockito.when(albumRepository.getAlbumsByArtist(anyInt())).thenReturn(expectedAlbums);

        //Act
        List<Album> actualAlbums = albumService.getAlbumsByArtist(anyInt());

        //Assert
        Assert.assertSame(expectedAlbums.size(), actualAlbums.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumsByArtistsShouldThrow_WhenArtistDoesNotExist() {
        //Act
        albumService.getAlbumsByArtist(anyInt());
    }

    @Test
    public void checkIfAlbumExistsShould_CallRepository() {
        //Arrange
        Album album = createAlbum();
        Mockito.when(albumRepository.getAlbumByTitle(album.getTitle()))
                .thenReturn(album);
        //Act
        albumService.checkIfAlbumExists(album.getTitle());

        //Assert
        Mockito.verify(albumRepository, times(1))
                .getAlbumByTitle(album.getTitle());
    }

    @Test
    public void uploadArtistAlbumsShould_CallRepository(){
        //Arrange
        Artist artist = createArtist();
        String url = "some url";
        Mockito.when(restTemplate.getForObject(url, String.class))
                .thenReturn(albumsResponse());
        Mockito.when(genreService.checkIfGenreExists(anyInt()))
                .thenReturn(true);

        //Act
        albumService.uploadArtistAlbums(url, artist.getId());

        //Assert
        Mockito.verify(albumRepository, times(1))
                .save(any(Album.class));
    }
}
