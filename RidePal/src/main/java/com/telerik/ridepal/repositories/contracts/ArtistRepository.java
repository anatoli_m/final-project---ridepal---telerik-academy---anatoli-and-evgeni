package com.telerik.ridepal.repositories.contracts;

import com.telerik.ridepal.models.Artist;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

public interface ArtistRepository extends CrudRepository<Artist, Serializable> {

    @Query("SELECT а FROM Artist а WHERE а.name = ?1")
    Artist getArtistByName(String name);
}
