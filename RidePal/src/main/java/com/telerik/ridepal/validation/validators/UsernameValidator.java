package com.telerik.ridepal.validation.validators;

import com.telerik.ridepal.validation.ValidUsername;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class UsernameValidator
        implements ConstraintValidator<ValidUsername, String> {

    private Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{4,20}$");

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        return pattern.matcher(username).matches();
    }
}