package com.telerik.ridepal.repositories.contracts;

import com.telerik.ridepal.models.Track;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;


public interface TrackRepository extends CrudRepository<Track, Serializable> {

    @Query("SELECT t FROM Track t WHERE t.title = ?1")
    List<Track> getTracksByTitle(String title);

    @Query("SELECT t FROM Track t WHERE t.artistId = ?1")
    List<Track> getTracksByArtist(int id);
}
