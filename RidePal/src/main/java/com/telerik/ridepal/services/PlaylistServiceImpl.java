package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.Track;
import com.telerik.ridepal.repositories.contracts.PlaylistRepository;
import com.telerik.ridepal.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    public static final String PLAYLIST_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Playlist with id %d does not exists.";
    public static final String PLAYLIST_WITH_NAME_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Playlist with name '%s' does not exists.";
    public static final String PLAYLIST_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Playlist with id %d already exists.";
    public static final String PLAYLIST_WITH_TITLE_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Playlist with title '%s' already exists.";
    public static final String NO_PLAYLISTS_EXCEPTION_MESSAGE = "There are no playlists";

    private PlaylistRepository playlistRepository;
    private RestTemplate restTemplate;
    private EntityManager entityManager;

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository playlistRepository,
                               RestTemplate restTemplate,
                               EntityManager entityManager) {
        this.playlistRepository = playlistRepository;
        this.restTemplate = restTemplate;
        this.entityManager = entityManager;
    }

    @Override
    public Playlist createPlaylist(Playlist playlist) {
        throwIfPlaylistExists(playlist.getPlaylist_id());
        playlistRepository.save(playlist);
        return playlist;
    }

    @Override
    public Playlist updatePlaylist(Playlist playlist) {
        throwIfPlaylistDoesNotExists(playlist.getPlaylist_id());
        playlistRepository.save(playlist);
        return playlist;
    }

    @Override
    public void deletePlaylist(int id) {
        throwIfPlaylistDoesNotExists(id);
        playlistRepository.deleteById(id);
    }

    @Override
    public String GenerateCoverForPlaylist() {
        String cover = "";

        try {
            URL url = new URL("https://source.unsplash.com/1920x1080/?music");
            BufferedImage bImage = ImageIO.read(url);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "jpg", bos);

            byte[] pictureBytes = bos.toByteArray();
            cover = Base64.getEncoder().encodeToString(pictureBytes);
            return cover;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cover;
    }

    @Override
    public List<Playlist> getPlaylists() {
        List<Playlist> playlists = new ArrayList<>();
        playlistRepository.findAll().forEach(playlists::add);
        return playlists;
    }

    @Override
    public List<Playlist> getPublicPlaylists(int userId) {
        List<Playlist> playlists = new ArrayList<>();
        playlists = playlistRepository.getPublicPlaylists(userId);
        return playlists;
    }

    @Override
    public Playlist getPlaylistById(int id) {
        throwIfPlaylistDoesNotExists(id);
        return playlistRepository.getPlaylistsById(id);
    }

    @Override
    public List<Playlist> getPlaylistByTitle(String title) {
        throwIfPlaylistDoesNotExists(title);
        List<Playlist> playlist = playlistRepository.getPlaylistsByTitle(title);
        return playlist;
    }

    @Override
    public List<Playlist> getPlaylistByUser(int id) {
        List<Playlist> playlists = new ArrayList<>();
        playlists = playlistRepository.getPlaylistsByUser(id);
        return playlists;
    }

    @Override
    public List<Playlist> getUserSavedPlaylists(int userID) {
        List<Playlist> playlists = new ArrayList<>();
        playlists = playlistRepository.getSavedPlaylists(userID);
        return playlists;
    }

    @Override
    public List<Playlist> getPlaylistsByRank(int rank) {
        List<Playlist> playlists = new ArrayList<>();
        playlistRepository.findAll().forEach(playlist -> {
            if (playlist.getRank() == rank) {
                playlists.add(playlist);
            }
        });
        if (playlists.size() == 0) {
            throw new EntityNotFoundException("Can't find Playlist with Rank "
                    + rank);
        }
        return playlists;
    }

    @Override
    public List<Track> getTracks(int playlistId) {
        throwIfPlaylistDoesNotExists(playlistId);
        List<Track> tracks = playlistRepository.getTracks(playlistId);
        if (tracks.size() == 0) {
            throw new EntityNotFoundException("Can't find Tracks with Playlist id "
                    + playlistId);
        }
        return tracks;
    }

    @Override
    public boolean checkIfPlaylistExists(int id) {
        return playlistRepository.existsById(id);
    }

    @Override
    public boolean checkIfPlaylistExists(String title) {

        return playlistRepository.getPlaylistsByTitle(title) != null;
    }

    public List<Track> generatePlaylist(HashMap<String, Integer> genres, double totalDuration,
                                        boolean distinctArtists, boolean includeRemixes, boolean includeLivePerformances, boolean topTracks) {
        List<Track> playlistTracks = new ArrayList<>();

        for (Map.Entry<String, Integer> entry : genres.entrySet()) {
            String genre = entry.getKey();
            double duration = totalDuration * (entry.getValue() / 100.0);
            if (entry.getValue() != 0) {
                List<Track> subPlaylist = generatePlaylist(genre,
                        (int) duration,
                        includeRemixes,
                        includeLivePerformances,
                        topTracks,
                        distinctArtists);
                playlistTracks.addAll(subPlaylist);
            }
        }

        Collections.shuffle(playlistTracks);
        return playlistTracks;
    }

    private List<Track> generatePlaylist(String genre,
                                         int duration,
                                         boolean includeRemixes,
                                         boolean includeLivePerformances,
                                         boolean topTracksOnly,
                                         boolean allowSameArtist) {
        return entityManager.createNamedStoredProcedureQuery("generatePlaylist")
                .setParameter("genre_name", genre)
                .setParameter("travel_duration", duration)
                .setParameter("include_remixes", includeRemixes)
                .setParameter("include_live_performances", includeLivePerformances)
                .setParameter("top_tracks_only", topTracksOnly)
                .setParameter("allow_same_artist", allowSameArtist)
                .getResultList();
    }

    private void throwIfPlaylistDoesNotExists(int id) {
        if (!checkIfPlaylistExists(id)) {
            throw new EntityNotFoundException(
                    String.format(PLAYLIST_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfPlaylistDoesNotExists(String title) {
        if (!checkIfPlaylistExists(title)) {
            throw new EntityNotFoundException(
                    String.format(PLAYLIST_WITH_NAME_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, title));
        }
    }

    private void throwIfPlaylistExists(int id) {
        if (checkIfPlaylistExists(id)) {
            throw new DuplicateEntityException(
                    String.format(PLAYLIST_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }
}
