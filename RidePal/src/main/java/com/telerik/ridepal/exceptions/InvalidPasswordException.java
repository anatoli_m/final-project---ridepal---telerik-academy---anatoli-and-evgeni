package com.telerik.ridepal.exceptions;

public class InvalidPasswordException extends RuntimeException {

    public InvalidPasswordException(String message) {
        super((message));
    }
}
