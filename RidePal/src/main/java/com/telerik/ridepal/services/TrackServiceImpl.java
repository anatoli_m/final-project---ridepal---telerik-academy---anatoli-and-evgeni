package com.telerik.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Album;
import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.models.Track;
import com.telerik.ridepal.repositories.contracts.TrackRepository;
import com.telerik.ridepal.services.contracts.TrackService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    public static final String TRACK_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Track with id %d does not exists.";
    public static final String TRACK_WITH_TITLE_DOES_NOT_EXISTS_EXCEPTION_MESSAGE =
            "Track with name '%s' does not exists.";
    public static final String TRACK_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Track with id %d already exists.";
    public static final String TRACK_WITH_TITLE_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "Track with title '%s' already exists.";
    public static final String NO_TRACKS_EXCEPTION_MESSAGE = "There are no tracks";

    private TrackRepository trackRepository;
    private RestTemplate restTemplate;

    @Autowired
    public TrackServiceImpl(TrackRepository trackRepository,
                            RestTemplate restTemplate) {
        this.trackRepository = trackRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public void uploadTracks(List<Track> tracks) {
        trackRepository.saveAll(tracks);
    }

    @Override
    public void uploadTracksFromAlbum(String url, Album album) {
        List<Track> tracks = fetchTracksFromUrl(url);
        tracks.forEach(track -> {
            track.setAlbumId(album.getId());
            track.setArtistId(album.getArtist_id());
        });
        trackRepository.saveAll(tracks);
    }

    @Override
    public Track createTrack(Track track) {
        throwIfTrackExists(track.getId());
        trackRepository.save(track);
        return track;
    }

    @Override
    public Track updateTrack(Track track) {
        throwIfTrackDoesNotExists(track.getId());
        trackRepository.save(track);
        return track;
    }

    @Override
    public void deleteTrack(int id) {
        throwIfTrackDoesNotExists(id);
        trackRepository.deleteById(id);
    }

    @Override
    public List<Track> getTracks() {
        List<Track> tracks = new ArrayList<>();
        trackRepository.findAll().forEach(tracks::add);
        if (tracks.isEmpty()) {
            throw new EntityNotFoundException(NO_TRACKS_EXCEPTION_MESSAGE);
        }
        return tracks;
    }

    @Override
    public Track getTrackById(int id) {
        throwIfTrackDoesNotExists(id);
        return trackRepository.findById(id).get();
    }

    @Override
    public List<Track> getTracksByTitle(String title) {
        throwIfTrackDoesNotExists(title);
        return trackRepository.getTracksByTitle(title);
    }

    @Override
    public List<Track> getTrackByArtist(int id) {
        List<Track> tracks = trackRepository.getTracksByArtist(id);
        if (tracks.isEmpty()) {
            throw new EntityNotFoundException(NO_TRACKS_EXCEPTION_MESSAGE);
        }
        return tracks;
    }

    @Override
    public boolean checkIfTrackExists(int id) {
        return trackRepository.existsById(id);
    }

    @Override
    public boolean checkIfTrackExists(String title) {
        return trackRepository.getTracksByTitle(title) != null;
    }

    private List<Track> fetchTracksFromUrl(String url) {
        String response = restTemplate.getForObject(url, String.class);
        JSONObject jsonObject = new JSONObject(response);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Track> tracks = new ArrayList<>();
        try {
            tracks = objectMapper
                    .readValue(jsonObject.get("data").toString(), new TypeReference<List<Track>>() {
                    });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return tracks;
    }

    private void throwIfTrackDoesNotExists(int id) {
        if (!checkIfTrackExists(id)) {
            throw new EntityNotFoundException(
                    String.format(TRACK_WITH_ID_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }

    private void throwIfTrackDoesNotExists(String title) {
        if (!checkIfTrackExists(title)) {
            throw new EntityNotFoundException(
                    String.format(TRACK_WITH_TITLE_DOES_NOT_EXISTS_EXCEPTION_MESSAGE, title));
        }
    }

    private void throwIfTrackExists(int id) {
        if (checkIfTrackExists(id)) {
            throw new DuplicateEntityException(
                    String.format(TRACK_WITH_ID_ALREADY_EXISTS_EXCEPTION_MESSAGE, id));
        }
    }
}
