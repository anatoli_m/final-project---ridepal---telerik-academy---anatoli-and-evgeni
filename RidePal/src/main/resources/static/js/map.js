
function loadMapScenario(startX, startY, endX, endY) {
    var myStyle = {
        "version": "1.0",
        "elements": {
            "mapElement": {
                "labelVisible": false,
            },
            "area": {
                "visible": false
            },
            "transportation": {
                "visible": false
            },
            "countryRegion": {
                "borderStrokeColor": "#444444",
                "borderOutlineColor": "#00000000",
                "fillColor": "#888888",
                "visible": true
            },
            "adminDistrict": {
                "borderVisible": false
            },
            "water": {
                "fillColor": "#FFFFFF"
            },
            "point": {
                "visible": false
            },
            "road": {
                "visible": false,
            }
        }
    };
    const centroidX = (startX + endX) / 2;
    const centroidY = (startY + endY) / 2;
    var map = new Microsoft.Maps.Map(document.getElementById('myMap2'),
        {
            center: new Microsoft.Maps.Location(centroidX, centroidY),
            zoom: 25,
            customMapStyle: myStyle,
            showZoomButtons: false,
            showBreadcrumb: false,
            showLocateMeButton: false,
            showMapTypeSelector: false,
            showScalebar: false,
            disableBirdseye: true,
            enableClickableLogo: false,
        });

    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);
        directionsManager.setRequestOptions({routeMode: Microsoft.Maps.Directions.RouteMode.driving});
        var waypoint1 = new Microsoft.Maps.Directions.Waypoint({
            address: 'Sofia',
            location: new Microsoft.Maps.Location(startX, startY)
        });
        var waypoint2 = new Microsoft.Maps.Directions.Waypoint({
            address: 'Plovdiv',
            location: new Microsoft.Maps.Location(endX, endY)
        });
        directionsManager.addWaypoint(waypoint1);
        directionsManager.addWaypoint(waypoint2);
        directionsManager.calculateDirections();
    });



}