package com.telerik.ridepal.controllers.rest;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Album;
import com.telerik.ridepal.services.contracts.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/albums")
public class AlbumsRestController {

    private AlbumService albumService;

    @Autowired
    public AlbumsRestController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping
    public List<Album> getAllAlbums() {
        List<Album> albums = new ArrayList<>();
        try {
            albums = albumService.getAlbums();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return albums;
    }

    @GetMapping("/{id}")
    public Album getAlbumById(@PathVariable int id) {
        Album album = new Album();
        try {
            album = albumService.getAlbum(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return album;
    }

    @PostMapping
    public Album createAlbum(@RequestBody Album album) {
        try {
            albumService.createAlbum(album);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return album;
    }

    @PutMapping
    public Album updateAlbum(@RequestBody Album album) {
        try {
            albumService.updateAlbum(album);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return album;
    }

    @GetMapping("/artist/{id}")
    public List<Album> getAlbumsByArtist(@PathVariable int id) {
        List<Album> albums = new ArrayList<>();
        try {
            albums = albumService.getAlbumsByArtist(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return albums;
    }

    @DeleteMapping("/{id}")
    public void deleteAlbum(@PathVariable int id) {
        try {
            albumService.deleteAlbum(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
