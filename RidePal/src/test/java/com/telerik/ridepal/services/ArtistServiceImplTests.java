package com.telerik.ridepal.services;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.repositories.contracts.ArtistRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.ridepal.Factory.artistsResponse;
import static com.telerik.ridepal.Factory.createArtist;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ArtistServiceImplTests {

    @Mock
    ArtistRepository artistRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    ArtistServiceImpl artistService;

    @Test
    public void createArtistShould_CallRepository() {
        //Arrange
        Artist artist = createArtist();

        //Act
        artistService.createArtist(artist);

        //Assert
        Mockito.verify(artistRepository, times(1)).save(artist);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createArtistShould_Throw_WhenArtistNameAlreadyExists() {
        //Arrange
        Artist artist = createArtist();
        Mockito.when(artistRepository.getArtistByName(anyString())).thenReturn(artist);

        //Act
        artistService.createArtist(artist);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createArtistShould_Throw_WhenArtistIdAlreadyExists() {
        //Arrange
        Artist artist = createArtist();
        Mockito.when(artistRepository.existsById(anyInt())).thenReturn(true);

        //Act
        artistService.createArtist(artist);
    }

    @Test
    public void createArtistShould_CreateArtist() {
        //Arrange
        Artist expectedArtist = createArtist();
        Mockito.when(artistRepository.save(expectedArtist)).thenReturn(expectedArtist);

        //Act
        Artist actualArtists = artistService.createArtist(expectedArtist);

        //Assert
        Assert.assertSame(expectedArtist, actualArtists);
    }

    @Test
    public void deleteArtistShould_CallRepository() {
        //Arrange
        Artist artist = createArtist();
        Mockito.when(artistRepository.existsById(artist.getId())).thenReturn(true);
        //Act

        artistService.deleteArtist(artist.getId());

        //Assert
        Mockito.verify(artistRepository, times(1)).deleteById(artist.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteArtistShould_Throw_WhenArtistDoesNotExist() {
        //Act
        artistService.deleteArtist(anyInt());
    }

    @Test
    public void getAllArtistsShould_CallRepository() {
        //Arrange
        List<Artist> artists = new ArrayList<>();
        Artist artist = createArtist();
        artists.add(artist);
        Mockito.when(artistRepository.findAll()).thenReturn(artists);

        //Act
        artistService.getArtists();

        //Assert
        Mockito.verify(artistRepository, times(1)).findAll();
    }

    @Test(expected = EntityNotFoundException.class)
    public void getArtistsShould_Throw_WhenNoArtists() {
        //Act
        artistService.getArtists();
    }

    @Test
    public void getArtistsShould_ReturnArtists() {
        //Arrange
        List<Artist> expectedArtists = new ArrayList<>();
        Artist artist = createArtist();
        expectedArtists.add(artist);
        Mockito.when(artistRepository.findAll()).thenReturn(expectedArtists);

        //Act
        List<Artist> actualArtists = artistService.getArtists();

        //Assert
        Assert.assertSame(expectedArtists.size(), actualArtists.size());
    }

    @Test
    public void getArtistByNameShould_CallRepository() {
        //Arrange
        Artist artist = createArtist();
        Mockito.when(artistRepository.getArtistByName(artist.getName())).thenReturn(artist);

        //Act
        artistService.getArtist(artist.getName());

        //Assert
        Mockito.verify(artistRepository, times(2)).getArtistByName(artist.getName());
    }

    @Test
    public void getArtistByNameShould_ReturnArtist() {
        //Arrange
        Artist expectedArtist = createArtist();
        Mockito.when(artistRepository.getArtistByName(expectedArtist.getName()))
                .thenReturn(expectedArtist);

        //Act
        Artist actualArtist = artistService.getArtist(expectedArtist.getName());

        //Assert
        Assert.assertSame(expectedArtist, actualArtist);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getArtistByNameShould_Throw_WhenArtistsDoesNotExist() {
        //Act
        artistService.getArtist(createArtist().getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getArtistByIdShould_Throw_WhenArtistsDoesNotExist() {
        //Act
        artistService.getArtist(createArtist().getId());
    }

    @Test
    public void checkIfArtistExistsByIdShould_CallRepository() {
        //Act
        artistService.checkIfArtistExists(anyInt());

        //Assert
        Mockito.verify(artistRepository, times(1)).existsById(anyInt());
    }

    @Test
    public void checkIfArtistExistsByNameShould_CallRepository() {
        //Act
        artistService.checkIfArtistExists(anyString());

        //Assert
        Mockito.verify(artistRepository, times(1)).getArtistByName(anyString());
    }

    @Test
    public void updateArtistShould_CallRepository(){
        //Arrange
        Artist artist = createArtist();
        Mockito.when(artistService.checkIfArtistExists(artist.getId()))
                .thenReturn(true);

        //Act
        artistService.updateArtist(artist);

        //Assert
        Mockito.verify(artistRepository, times(1))
                .save(artist);
    }

    @Test
    public void uploadArtistsShould_CallRepository(){
        //Arrange
        String url = "some url";
        Mockito.when(restTemplate.getForObject(url, String.class))
                .thenReturn(artistsResponse());

        //Act
        artistService.uploadArtists(url);

        //Assert
        Mockito.verify(artistRepository, times(1))
                .saveAll(any(List.class));
    }
}
