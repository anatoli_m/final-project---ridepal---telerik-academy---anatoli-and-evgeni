package com.telerik.ridepal.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MapController {

    @GetMapping("/map")
    public String showMap(Model model) {

        String url = "https://www.bing.com/api/maps/mapcontrol?key="
                + System.getenv("BingApiKey")
                + "&callback=loadMapScenario";
        model.addAttribute("url", url);

        return "generate";
    }
}
