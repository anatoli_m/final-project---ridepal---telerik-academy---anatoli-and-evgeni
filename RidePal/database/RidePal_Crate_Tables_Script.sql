DROP DATABASE IF EXISTS RidePalDB;

CREATE DATABASE RidePalDB;

USE RidePalDB;

CREATE TABLE users (
user_id INT AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(20) NOT NULL UNIQUE,
password VARCHAR(200) NOT NULL, 
first_name VARCHAR(50) NOT NULL,
last_name VARCHAR(50) NOT NULL,
email VARCHAR(50) NOT NULL UNIQUE,
avatar MEDIUMTEXT,
enabled BOOLEAN NOT NULL DEFAULT 1
);

CREATE TABLE roles (
role_id INT AUTO_INCREMENT PRIMARY KEY,
role VARCHAR(20)
);

CREATE TABLE user_roles (
user_role_id INT AUTO_INCREMENT PRIMARY KEY,
user_id INT NOT NULL,
role_id INT NOT NULL,

FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

CREATE TABLE genres(
   genre_id INT AUTO_INCREMENT PRIMARY KEY,
   genre VARCHAR(30) NOT NULL,
   picture VARCHAR(200) NOT NULL,
   enabled BOOLEAN NOT NULL DEFAULT 1
);

CREATE TABLE artists(
   artist_id INT AUTO_INCREMENT PRIMARY KEY,
   artist_name VARCHAR(200) NOT NULL,
   picture VARCHAR(200) NOT NULL,
   tracklist VARCHAR(200),
   enabled BOOLEAN NOT NULL DEFAULT 1
);

CREATE TABLE albums (
album_id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(200) NOT NULL,
tracklist VARCHAR(200) NOT NULL, 
cover VARCHAR(200),
genre_id INT NOT NULL,
artist_id INT NOT NULL,
release_date DATE, 
enabled BOOLEAN NOT NULL DEFAULT 1,

FOREIGN KEY (genre_id) REFERENCES genres(genre_id),
FOREIGN KEY (artist_id) REFERENCES artists(artist_id)
);

CREATE TABLE tracks (
track_id INT AUTO_INCREMENT PRIMARY KEY, 
title VARCHAR(200) NOT NULL, 
link VARCHAR(200) NOT NULL,
duration INT NOT NULL,
artist_id INT NOT NULL,
album_id INT NOT NULL,
rank INT NOT NULL,
preview VARCHAR(200) NOT NULL,
enabled BOOLEAN NOT NULL DEFAULT 1,

FOREIGN KEY (artist_id) REFERENCES artists(artist_id),
FOREIGN KEY (album_id) REFERENCES albums(album_id)
);

CREATE TABLE playlists (
playlist_id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(20) NOT NULL,
description VARCHAR(200),
duration INT NOT NULL,
rank INT NOT NULL,
user_id INT NOT NULL,
cover MEDIUMTEXT,
private BOOLEAN NOT NULL DEFAULT 1, 
enabled BOOLEAN NOT NULL DEFAULT 1,

FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE playlist_tracks(
playlist_track_id INT AUTO_INCREMENT PRIMARY KEY,
playlist_id INT NOT NULL,
track_id INT NOT NULL,

FOREIGN KEY (playlist_id) REFERENCES playlists(playlist_id),
FOREIGN KEY (track_id) REFERENCES tracks(track_id)
);

CREATE TABLE playlist_genres (
playlist_genre_id INT AUTO_INCREMENT PRIMARY KEY,
playlist_id INT NOT NULL, 
genre_id INT NOT NULL,

FOREIGN KEY (playlist_id) REFERENCES playlists(playlist_id),
FOREIGN KEY (genre_id) REFERENCES genres(genre_id)
);

CREATE TABLE saved_playlists(
saved_playlist_id INT AUTO_INCREMENT PRIMARY KEY,
user_id INT NOT NULL, 
playlist_id INT NOT NULL,

FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (playlist_id) REFERENCES playlists(playlist_id)
);

ALTER TABLE genres CONVERT TO CHARACTER SET UTF8;
ALTER TABLE artists CONVERT TO CHARACTER SET UTF8;
ALTER TABLE albums CONVERT TO CHARACTER SET UTF8;
ALTER TABLE tracks CONVERT TO CHARACTER SET UTF8;
ALTER TABLE users CONVERT TO CHARACTER SET UTF8;
ALTER TABLE playlists CONVERT TO CHARACTER SET UTF8;


USE `ridepaldb`;
DROP procedure IF EXISTS `generate_playlist`;

DELIMITER $$
USE `ridepaldb`$$
CREATE PROCEDURE `generate_playlist` (IN genre_name VARCHAR(20), 
										IN travel_duration INT,
                                        IN include_remixes BOOLEAN,
                                        IN include_live_performances BOOLEAN,
                                        IN top_tracks_only BOOLEAN,
                                        IN allow_same_artist BOOLEAN)
BEGIN

SET @sum = 0;

SELECT * 
FROM (SELECT *, (@sum := @sum + duration) AS total_duration
		FROM (SELECT tracks.* FROM tracks 
			JOIN albums ON (albums.album_id = tracks.album_id)
			JOIN genres ON (genres.genre_id = albums.genre_id)
			WHERE genre LIKE genre_name
			AND IF (include_remixes, tracks.title LIKE '%%',  tracks.title NOT LIKE '%Remix%')
			AND IF (include_live_performances, tracks.title LIKE '%%', tracks.title NOT LIKE '%Live%')
			AND IF (top_tracks_only, tracks.rank > (SELECT AVG(tracks.rank)*1.5 FROM tracks), tracks.rank >= 0)
			-- AND albums.release_date NOT BETWEEN '2010-01-01' AND '2010-12-31'
			AND tracks.duration BETWEEN 90 AND 600
			GROUP BY CASE WHEN (allow_same_artist) 
				THEN tracks.track_id
				ELSE tracks.artist_id END
			ORDER BY RAND() 
			LIMIT 1000) 
			AS random_tracks) 
        AS playlist
WHERE total_duration <= travel_duration + 300;

END$$

DELIMITER ;

