package com.telerik.ridepal.controllers;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.models.DTO.DtoMapper;
import com.telerik.ridepal.models.DTO.UserDto;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;
    private DtoMapper dtoMapper;

    @Autowired
    public RegistrationController(UserService userService,
                                  DtoMapper dtoMapper) {
        this.userService = userService;
        this.dtoMapper = dtoMapper;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") UserDto userDto,
                               BindingResult bindingResult,
                               Model model) {
        try {
            if (bindingResult.hasErrors()) {
                model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
                model.addAttribute("user", userDto);
                return "register";
            }
            User user = dtoMapper.userFromDto(userDto);
            userService.createUser(user);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("user", userDto);
            return "register";
        }

        return "redirect:/login?success";
    }
}
