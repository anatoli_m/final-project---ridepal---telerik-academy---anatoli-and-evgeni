$(document).ready(

    function () {

        //Generate overview
        $("#infoNextBtn").click(function () {
            $("#list-overview").append("<li class=\"list-group-item\">Playlist's name: " + $("#playlistTitle").val() +"</li>\n" +
                "<li class=\"list-group-item\">Playlist's description: " + $("#playlistDescription").val() + "</li>\n")

        });

        $("#distinctArtists").click(function () {
            $("#list-overview2").append("<li class=\"list-group-item\">Tracks from same artists: " + ' <i class="fas fa-check"></i>' +"</li>\n");
        });

        $("#topTracks").click(function () {
            $("#list-overview2").append("<li class=\"list-group-item\">Top tracks: " + ' <i class="fas fa-check"></i>' +"</li>\n");
        });

        $("#includeRemixes").click(function () {
            $("#list-overview2").append("<li class=\"list-group-item\">Include remixes: " + ' <i class="fas fa-check"></i>' +"</li>\n");
        });

        $("#includeLivePerformance").click(function () {
            $("#list-overview2").append("<li class=\"list-group-item\">Include live performance: " + ' <i class="fas fa-check"></i>' +"</li>\n");
        });

        $("#makePublic").click(function () {
            $("#list-overview2").append("<li class=\"list-group-item\">Make playlist public: " + ' <i class="fas fa-check"></i>' +"</li>\n");
        });


        $("#genrePrevBtn").click(function () {
            // $("#list-overview").empty();
            $('#list-overview li').slice(-2).remove();
        });

        $("#btn-overview").click(function () {

            if(parseInt($("#rangeHipHop").val()) > 0  ) {
                $("#list-overview").append("<li class=\"list-group-item\" id='resultHipHop'>Hip-Hop/Rap: " + $("#chooseHipHop").text() +"</li>\n");
            } else if(parseInt($("#rangeHipHop").val()) === 0 ) {
                $("#resultHipHop").remove();
            }

            if(parseInt($("#rangePop").val()) > 0  ) {
                $("#list-overview2").append("<li class=\"list-group-item\" id='resultPop'>Pop: " + $("#choosePop").text() +"</li>\n");
            } else if(parseInt($("#rangePop").val()) === 0 ) {
                $("#resultPop").remove();
            }

            if(parseInt($("#rangeMetal").val()) > 0  ) {
                $("#list-overview").append("<li class=\"list-group-item\" id='resultMetal'>Metal: " + $("#chooseMetal").text() +"</li>\n");
            } else if(parseInt($("#rangeMetal").val()) === 0 ) {
                $("#resultMetal").remove();
            }

            if(parseInt($("#rangeHouse").val()) > 0  ) {
                $("#list-overview2").append("<li class=\"list-group-item\" id='resultHouse'>Jazz: " + $("#chooseHouse").text() +"</li>\n");
            } else if(parseInt($("#rangeHouse").val()) === 0 ) {
                $("#resultHouse").remove();
            }
        });


        $(document).ready(function () {
            $('#getLocationBtn').attr('disabled', true).text("Enter destinations to continue");
            $('#startDestination, #endDestination').on('keyup', function () {
                var start = $("#startDestination").val();
                var end = $('#endDestination').val();
                if (start !== '' && end !== '') {
                    $('#getLocationBtn').attr('disabled', false).text("Next");
                } else {
                    $('#getLocationBtn').attr('disabled', true).text("Enter destinations to continue");
                }
            });
        });

        $(document).ready(function () {
            $('#infoNextBtn').attr('disabled', true).text("Enter details to continue");
            $('#playlistTitle, #playlistDescription').on('keyup', function () {
                var title = $("#playlistTitle").val();
                var descritpion = $('#playlistDescription').val();
                if (title !== '' && descritpion !== '') {
                    $('#infoNextBtn').attr('disabled', false).text("Next");
                } else {
                    $('#infoNextBtn').attr('disabled', true).text("Enter details to continue");
                }
            });
        });

        //Get location

        $("#getLocationBtn").click(function (event) {

            if($("#startDestination").val() === $("#endDestination").val()) {
                setTimeout(function(){location.href="/generate"} , 1000);
                $.busyLoadSetup({ animation: "slide", background: "rgba(255,0,30,0.86)" });

                $.busyLoadFull("show", {
                    spinner: "accordion",
                    text: "Invalid location input ...",
                    fontSize: "2rem"
                });

            }
            // Prevent the form from submitting via the browser.
            event.preventDefault();

            $.busyLoadSetup({ animation: "slide", background: "rgba(33,0,255,0.86)" });

            $.busyLoadFull("show", {
                spinner: "accordion",
                text: "Calculating destination ...",
                fontSize: "2rem"
            });


            ajaxDestination();
        });

        // SUBMIT FORM
        $("#generateForm").submit(function (event) {
            // Prevent the form from submitting via the browser.
            event.preventDefault();
            $.busyLoadSetup({ animation: "slide", background: "rgba(33,0,255,0.86)" });

            $.busyLoadFull("show", {
                spinner: "accordion",
                text: "Generating your personal playlist ...",
                fontSize: "2rem"
            });
            ajaxPost();
        });

        function ajaxDestination() {
            $.getScript("http://localhost:8080/js/map.js");
            // PREPARE FORM DATA
            const formData = {
                fromDestination: $("#startDestination").val(),
                toDestination: $("#endDestination").val(),
            };


            // DO POST
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                url: "/getLocation",
                data: JSON.stringify(formData),
                dataType: 'json',
                success: function (result) {
                    $.busyLoadFull("hide");
               const duration = result.duration;
               const endX = result.centroid[0];
               const endY =result.centroid[1];
               const startX = result.centroid[2];
               const startY = result.centroid[3];
                    $("#list-overview").empty();
                    $("#list-overview").append(
                        "<li class=\"list-group-item\">Trip duration: " + sec2time(duration) +"</li>")
                loadMapScenario(endX, endY, startX, startY);
                },
                error: function(xhr, status, error){
                    $.busyLoadFull("hide");
                    $.busyLoadSetup({ animation: "slide", background: "rgba(255,0,30,0.86)" });

                    $.busyLoadFull("show", {
                        spinner: "accordion",
                        text: "Invalid location input ...",
                        fontSize: "2rem"
                    });

                    setTimeout(function(){location.href="/generate"} , 3000);

                }
            });
        }

        function ajaxPost() {

            // var form = [[${toDuration}]];
            // PREPARE FORM DATA
            const formData = {
                fromDestination: $("#startDestination").val(),
                toDestination: $("#endDestination").val(),
                title: $("#playlistTitle").val(),
                description: $("#playlistDescription").val(),
                distinctArtists: $("#distinctArtists").val(),
                includeRemixes: $("#includeRemixes").val(),
                includeLivePerformance: $("#includeLivePerformance").val(),
                makePublic: $("#makePublic").val(),
                topTracks: $("#topTracks").val(),
                    hiphop: parseInt($("#rangeHipHop").val()),
                    pop: parseInt($("#rangePop").val()),
                    metal: parseInt($("#rangeMetal").val()),
                    house: parseInt($("#rangeHouse").val()),
                };


            // DO POST
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                url: "/generate",
                data: JSON.stringify(formData),
                dataType: 'json',
                success: function (result) {
                    $.busyLoadFull("hide");
                    $("#multi-menu").fadeOut();
                    $("#generateForm").fadeOut();
                    // $("#playlist").css("display", "block", "!important");
                    $('#playlist').attr('style', 'display: flex !important');
                    $("#mainTitle").text("My playlist");
                    $("#generatedPlaylistTitle").append("<h2>" + result.title + "</h2>");
                    $("#generatedPlaylistTitle").append("<h6 id='generatedPlaylistDescription'>" + result.description + "</h6>");
                    $(".card-text").text(result.description);
                    $("#playlistDuration").text("Duration: " + sec2time(result.duration));
                    $("#playlistGenres").text("Genres: " + result.genres.name);

                    let tracks = [];
                    for (let i = 0; i < result.tracks.length ; i++) {
                        tracks.push({
                            title: result.tracks[i].title,
                            artist: "RidePal",
                            mp3: result.tracks[i].preview,
                            poster: 'data:image/png;base64,' + result.cover,
                            duration:sec2mins(result.tracks[i].duration)
                        });
                      // tracks.push(result.tracks[i].preview);
                    }

                    new jPlayerPlaylist({
                        jPlayer: "#player-seven",
                        cssSelectorAncestor: ".player-layout-seven"
                    }, tracks, {
                        swfPath: "../../dist/jplayer",
                        supplied: "webmv, ogv, m4v, oga, mp3",
                        useStateClassSkin: true,
                        autoBlur: false,
                        smoothPlayBar: true,
                        keyEnabled: true,
                        loop: true,
                        audioFullScreen: true
                    });


                    $("#playBtn").click(function () {
                        if($(this).text() === "Play") {
                            $("#playBtn").text("Stop");
                            autoplay(0, tracks);
                        } else {
                            $("#playBtn").text("Play");
                            sound.stop();
                        }

                    });



                        $('#tracks').append("<thead>\n" +
                            "<tr>\n" +
                            "<th scope=\"col\">Title</th>\n" +
                            "<th scope=\"col\">Duration</th>\n" +
                            "</tr>\n" +
                            "</thead>");

                    $.extend($.fn.dataTable.defaults, {
                        scrollY: 400,
                    });

                    $('#tracks').DataTable({
                        paging: false,
                        "processing": true,
                        "data": result.tracks,
                        "columns": [
                            { "data": "title" },
                            { "data": "duration" }
                        ]
                    });
                }
            });
        }
    });

function sec2time(timeInSeconds) {
    var pad = function(num, size) { return ('000' + num).slice(size * -1); },
        time = parseFloat(timeInSeconds).toFixed(3),
        hours = Math.floor(time / 60 / 60),
        minutes = Math.floor(time / 60) % 60,
        seconds = Math.floor(time - minutes * 60);

    return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
}

function sec2mins(timeInMins) {
    var pad = function(num, size) { return ('000' + num).slice(size * -1); },
        time = parseFloat(timeInMins).toFixed(3),
        hours = Math.floor(time / 60 / 60),
        minutes = Math.floor(time / 60) % 60,
        seconds = Math.floor(time - minutes * 60);

    return pad(minutes, 2) + ':' + pad(seconds, 2);
}

