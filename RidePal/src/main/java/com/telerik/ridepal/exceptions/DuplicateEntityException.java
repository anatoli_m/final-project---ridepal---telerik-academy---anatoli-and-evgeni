package com.telerik.ridepal.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String message) {
        super((message));
    }
}

