package com.telerik.ridepal.controllers;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.helpers.TimeCalculator;
import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.User;
import com.telerik.ridepal.repositories.contracts.PlaylistRepository;
import com.telerik.ridepal.services.contracts.PlaylistService;
import com.telerik.ridepal.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/browse")
public class BrowseController {
    private static final String NO_PLAYLISTS_ERROR_MESSAGE = "You haven't created any playlists yet";
    private static final String PLAYLIST_SAVED_MESSAGE = "Playlist saved!";

    private UserService userService;
    private PlaylistRepository playlistRepository;
    private PlaylistService playlistService;

    @Autowired
    public BrowseController(UserService userService, PlaylistRepository playlistRepository, PlaylistService playlistService) {
        this.userService = userService;
        this.playlistRepository = playlistRepository;
        this.playlistService = playlistService;
    }


    @GetMapping
    public String homePage(Model model, Principal principal) {
        List<Playlist> playlists = playlistRepository
                .getPublicPlaylists(userService.getUser(principal.getName()).getUser_id());
        model.addAttribute("playlists", playlists);
        TimeCalculator timeCalculator = new TimeCalculator();
        int totalPlayTime = 0;

        try {
            totalPlayTime = calculateTotalPlayTime(playlists);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", NO_PLAYLISTS_ERROR_MESSAGE);
        }

        User user = new User();
        try {
            user = userService.getUser(principal.getName());
            model.addAttribute("user", user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }

        model.addAttribute("timeCalculator", timeCalculator);

        return "browse";
    }

    @GetMapping("/save/{id}")
    public String savePlaylist(@PathVariable int id,
                               Principal principal,
                               Model model) {

        User user = new User();
        try {
            user = userService.getUser(principal.getName());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }

        Playlist playlist = new Playlist();
        try {
            playlist = playlistService.getPlaylistById(id);
            user.getSavedPlaylists().add(playlist);
            userService.updateUser(user);
            model.addAttribute("success", PLAYLIST_SAVED_MESSAGE);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }

        return "redirect:/browse";
    }


    private static int calculateTotalPlayTime(List<Playlist> list) {
        Iterator<Playlist> it = list.iterator();
        int res = 0;
        while (it.hasNext()) {
            int num = it.next().getDuration();
            res += num;
        }
        return res;
    }
}
