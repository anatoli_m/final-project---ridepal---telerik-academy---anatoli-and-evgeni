package com.telerik.ridepal.controllers.rest;

import com.telerik.ridepal.exceptions.DuplicateEntityException;
import com.telerik.ridepal.exceptions.EntityNotFoundException;
import com.telerik.ridepal.models.Genre;
import com.telerik.ridepal.services.DatabaseUpdateServiceImpl;
import com.telerik.ridepal.services.contracts.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/genres")
public class GenresRestController {

    private static final String GENRES_URL = "https://api.deezer.com/genre";

    GenreService genreService;
    DatabaseUpdateServiceImpl databaseInitializer;

    @Autowired
    public GenresRestController(GenreService genreService,
                                DatabaseUpdateServiceImpl databaseInitializer) {
        this.genreService = genreService;
        this.databaseInitializer = databaseInitializer;
    }

    @PutMapping
    void uploadGenres() {
        genreService.uploadGenres(GENRES_URL);
    }

    @GetMapping("/{name}")
    public Genre getGenreByName(@PathVariable String name) {
        return genreService.getGenreByName(name);
    }

    @GetMapping
    public List<Genre> getAllGenres() {
        List<Genre> genres = new ArrayList<>();
        try {
            genres = genreService.getGenres();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return genres;
    }

    @GetMapping("/{id}")
    public Genre getGenre(@PathVariable int id) {
        Genre genre = new Genre();
        try {
            genre = genreService.getGenreById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return genre;
    }

    @PostMapping
    public Genre createGenre(@RequestBody Genre genre) {
        try {
            genreService.createGenre(genre);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return genre;
    }
    @PutMapping("/{id}")
    public Genre updateGenre(@PathVariable int id, @RequestBody Genre genre) {
        try {
            genreService.updateGenre(genre);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return genre;
    }

    @DeleteMapping("/{id}")
    public void deleteGenre(@PathVariable int id) {
        try {
            genreService.deleteGenre(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
