package com.telerik.ridepal.services;

import com.telerik.ridepal.models.Album;
import com.telerik.ridepal.models.Artist;
import com.telerik.ridepal.services.contracts.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.ridepal.Factory.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UpdateDatabaseServiceImpl {

    @InjectMocks
    DatabaseUpdateServiceImpl databaseUpdateService;

    @Mock
    GenreService genreService;

    @Mock
    ArtistService artistService;

    @Mock
    AlbumService albumService;

    @Mock
    TrackService trackService;

    @Test
    public void updateGenresShould_CallGenreService() {
        //Act
        databaseUpdateService.updateGenres();

        //Assert
        Mockito.verify(genreService, times(1))
                .uploadGenres(anyString());
    }

    @Test
    public void updateArtistsShould_CallArtistService() {
        //Arrange
        Mockito.when(genreService.getGenreByName(anyString()))
                .thenReturn(createGenre());

        //Act
        databaseUpdateService.updateArtists();

        //Assert
        Mockito.verify(artistService, times(4))
                .uploadArtists(anyString());
    }

    @Test
    public void updateAlbumsShould_CallAlbumService() {
        //Arrange
        List<Artist> artists = new ArrayList<>();
        Artist artist = createArtist();
        artists.add(artist);
        Mockito.when(artistService.getArtists())
                .thenReturn(artists);
        String url = String.format("https://api.deezer.com/artist/%d/albums", artist.getId());

        //Act
        databaseUpdateService.updateAlbums();

        //Assert
        Mockito.verify(albumService, times(1))
                .uploadArtistAlbums(url, artist.getId());
    }

    @Test
    public void updateTracksShould_CallTracksService() {
        //Arrange
        List<Album> albums = new ArrayList<>();
        Album album = createAlbum();
        albums.add(album);
        Mockito.when(albumService.getAlbums()).thenReturn(albums);
        String url = String.format("https://api.deezer.com/album/%d/tracks", album.getId());

        //Act
        databaseUpdateService.updateTracks();

        //Assert
        Mockito.verify(trackService, times(1))
                .uploadTracksFromAlbum(url, album);
    }

    @Test
    public void updateDatabaseShould_CallAllRepositories() {
        //Arrange
        Mockito.when(genreService.getGenreByName(anyString()))
                .thenReturn(createGenre());
        List<Artist> artists = new ArrayList<>();
        Artist artist = createArtist();
        artists.add(artist);
        Mockito.when(artistService.getArtists())
                .thenReturn(artists);
        String url = String.format("https://api.deezer.com/artist/%d/albums", artist.getId());
        List<Album> albums = new ArrayList<>();
        Album album = createAlbum();
        albums.add(album);
        Mockito.when(albumService.getAlbums()).thenReturn(albums);
        String url2 = String.format("https://api.deezer.com/album/%d/tracks", album.getId());

        //Act
        databaseUpdateService.updateDatabase();

        //Assert
        Mockito.verify(genreService, times(1))
                .uploadGenres(anyString());
        Mockito.verify(artistService, times(4))
                .uploadArtists(anyString());
        Mockito.verify(albumService, times(1))
                .uploadArtistAlbums(url, artist.getId());
        Mockito.verify(trackService, times(1))
                .uploadTracksFromAlbum(url2, album);
    }
}
