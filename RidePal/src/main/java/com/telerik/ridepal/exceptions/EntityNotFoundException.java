package com.telerik.ridepal.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super((message));
    }
}
