$(document).ready(
    function () {

        // SUBMIT FORM
        $("#generateForm").submit(function (event) {
            // Prevent the form from submitting via the browser.
            event.preventDefault();
            ajaxPost();
            // $("#durationId").append("" + "Test" + "</p>")
        });

        function ajaxPost() {

            // var form = [[${toDuration}]];
            // PREPARE FORM DATA
            const formData = {
                fromDestination: $("#startDestination").val(),
                toDestination: $("#endDestination").val(),
                title: $("#playlistTitle").val(),
                description: $("#playlistDescription").val(),
                genre: $("#selectGenre").val()
            };

            // const playlistDto = {
            //     title: $("#playlistTitle").val(),
            //     description: $("#playlistDescription").val(),
            // };


            // DO POST
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                url: "/generate",
                data: JSON.stringify(formData),
                dataType: 'json',
                success: function (result) {
                    $("#generateForm").fadeOut();
                    $("#mainTitle").text("My playlist");
                    $("#durationId").html(
                        "Playlist title:" + result.title + " <br>" +
                        "Playlist description:" + result.description + " <br>" +
                        "Playlist duration:" + result.duration + "</p>");

                    $('#tracks').DataTable({
                        rowReorder: {
                            dataSrc: 'tr'
                        },
                        "processing": true,
                            "data": result.tracks,
                        "columns": [
                            { "data": "title" },
                            { "data": "duration" }
                        ]
                    });

                        // $.each(result.tracks,
                        // function(i, track) {
                        //     const tracksList = "Track Name  "
                        //         + track.title
                        //         + ", Duration  = " + track.duration
                        //         + "<br>";
                        //     $('#getResultDiv .list-group').append(
                        //         tracksList)
                        //     $('#tracks').append("<thead>\n" +
                        //         "<tr>\n" +
                        //         "<th scope=\"col\">#</th>\n" +
                        //         "<th scope=\"col\">Title</th>\n" +
                        //         "<th scope=\"col\">Duration</th>\n" +
                        //         "</tr>\n" +
                        //         "</thead>");

                        //     $.each(result.tracks,
                        //         function(i, track) {
                        //             const tracksList = "<tr>" + " <th scope=\"row\">"+ (i + 1) +"</th>" + "<td>"
                        //                 + track.title + "</td>"
                        //                 + "<td>" + track.duration + "</td>"
                        //                 + "<td>" + "<audio controls>\n" +
                        //                 "  <source src=\"" + track.preview + "\" type=\"audio/mpeg\">\n" +
                        //                 "Your browser does not support the audio element.\n" +
                        //                 "</audio>\n" + "</td>"
                        //                 + "</tr>";
                        //             $('#tracksTable').append(
                        //                 tracksList)
                        // });


                }
            });
        }
    });
