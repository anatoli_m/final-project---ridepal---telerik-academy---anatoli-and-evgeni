package com.telerik.ridepal.helpers;

import java.util.concurrent.TimeUnit;

public class TimeCalculator {

    public String calculateTime(long seconds) {
        int days = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (days * 24);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

        if (days != 0) {
            return String.format("%dd %dh", days, hours);
        } else if (hours != 0) {
            return String.format("%dh %dmin", hours, minutes);
        } else if (minutes != 0) {
            return String.format("%dmin", minutes);
        } else {
            return String.format("%ds", second);
        }
    }
}
