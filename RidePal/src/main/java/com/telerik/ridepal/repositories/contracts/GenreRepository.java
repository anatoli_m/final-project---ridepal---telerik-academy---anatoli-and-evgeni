package com.telerik.ridepal.repositories.contracts;

import com.telerik.ridepal.models.Genre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

public interface GenreRepository extends CrudRepository<Genre, Serializable> {

    @Query("SELECT g FROM Genre g WHERE g.name = ?1")
    Genre getGenreByName(String genreName);
}
