# Final Project - RidePal - Telerik Academy - Anatoli and Evgeni

# RidePal

**We have won the Golden Project Award from Telerik Academy!!!***

RidePal Playlist Generator enables users to generate playlists for specific travel duration periods based on their preferred genres.
 
A playlist consists of a list of individual tracks (songs). Each track has an artist, title, album, duration (playtime length) and rank (a numeric value). 
Each playlist has a user given title, associated tags (e.g. musical genres), a list of tracks with the associated track details, total playtime (the sum of the playtimes of all tracks in that playlist) and rank (the average of the ranks of all tracks in that playlist). 

**Designed and created for Telerik Academy, by Anatoli & Evgeni**
