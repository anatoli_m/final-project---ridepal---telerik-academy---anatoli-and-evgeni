package com.telerik.ridepal.services.contracts;

import com.telerik.ridepal.models.Playlist;
import com.telerik.ridepal.models.Track;

import java.util.HashMap;
import java.util.List;

public interface PlaylistService {

    Playlist createPlaylist(Playlist playlist);

    Playlist updatePlaylist(Playlist playlist);

    void deletePlaylist(int id);

    String GenerateCoverForPlaylist();

    List<Playlist> getPlaylists();

    List<Playlist> getPublicPlaylists(int userId);

    Playlist getPlaylistById(int id);

    List<Playlist> getPlaylistByTitle(String title);

    List<Playlist> getPlaylistByUser(int id);

    List<Playlist> getUserSavedPlaylists(int userID);

    List<Playlist> getPlaylistsByRank(int rank);

    List<Track> getTracks(int playlistId);

    boolean checkIfPlaylistExists(int id);

    boolean checkIfPlaylistExists(String title);

    List<Track> generatePlaylist(HashMap<String, Integer> genres, double totalDuration,
                                 boolean distinctArtists, boolean includeRemixes, boolean includeLivePerformances, boolean topTracks);
}
