package com.telerik.ridepal.services;

import com.telerik.ridepal.services.contracts.LocationService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


@Service
public class LocationServiceImpl implements LocationService {

    private static final String BING_LOCATION_API_URL =
            "http://dev.virtualearth.net/REST/v1/Routes/driving?wayPoint.1=%s&wayPoint.2=%s&distanceUnit=km&key=%s";

    private RestTemplate restTemplate;

    @Autowired
    public LocationServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build();
    }

    @Override
    public double getTravelDuration(String from, String to) throws JSONException {
        String bingApiKey = String.format(BING_LOCATION_API_URL,
                from,
                to,
                System.getenv("BingApiKey"));

        String response = restTemplate.getForObject(bingApiKey, String.class);

        JSONObject jsonObject = new JSONObject(response);

        return jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0).getDouble("travelDuration");
    }

    @Override
    public List<Double> getCentroid(String from, String to) throws JSONException {
        String bingApiKey = String.format(BING_LOCATION_API_URL,
                from,
                to,
                System.getenv("BingApiKey"));

        String response = restTemplate.getForObject(bingApiKey, String.class);

        JSONObject jsonObject = new JSONObject(response);

        List<Double> centroidCoordinates = new ArrayList<>();

        String endPoint = jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0).
                        getJSONArray("routeLegs").getJSONObject(0).
                        getJSONObject("actualEnd").getJSONArray("coordinates").toString();


        String startPoint = jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0).
                        getJSONArray("routeLegs").getJSONObject(0).
                        getJSONObject("actualStart").getJSONArray("coordinates").toString();

        endPoint= endPoint.replace("[", "");
        endPoint= endPoint.replace("]", "");
        startPoint= startPoint.replace("[", "");
        startPoint= startPoint.replace("]", "");

        double[] arrEnd = Stream.of(endPoint.split(","))
                .mapToDouble (Double::parseDouble)
                .toArray();

        double[] arrStart = Stream.of(startPoint.split(","))
                .mapToDouble (Double::parseDouble)
                .toArray();


        centroidCoordinates.add(arrEnd[0]);
        centroidCoordinates.add(arrEnd[1]);
        centroidCoordinates.add(arrStart[0]);
        centroidCoordinates.add(arrStart[1]);


        return centroidCoordinates;
    }
}
