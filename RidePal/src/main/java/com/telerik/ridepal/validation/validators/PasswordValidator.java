package com.telerik.ridepal.validation.validators;

import com.telerik.ridepal.validation.ValidPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PasswordValidator
        implements ConstraintValidator<ValidPassword, String> {

    private Pattern pattern = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$");
    //one lowercase, one uppercase and one number

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        return pattern.matcher(password).matches();
    }
}